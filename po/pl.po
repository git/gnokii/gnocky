# Polish translation of gnocky.
# Copyright (C) 2007 Pawel Kot
# This file is distributed under the same license as the gnocky package.
# Pawel Kot <gnokii@gmail.com>, 2007.
# 
#  <>, 2007.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: gnocky 0.0.6\n"
"Report-Msgid-Bugs-To: http://savannah.nongnu.org/projects/gnokii/\n"
"POT-Creation-Date: 2008-04-12 22:46+0200\n"
"PO-Revision-Date: 2007-12-04 13:43+0100\n"
"Last-Translator: Pawel Kot <gnokii@gmail.com>\n"
"Language-Team: PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/glade/gnocky-addressbook-list-dialog.glade:8
#: src/glade/gnocky-new-sms.glade:8
msgid "New SMS"
msgstr "Nowy SMS"

#: src/glade/gnocky-addressbook-list-dialog.glade:74
#: src/glade/gnocky-new-sms.glade:74
msgid "SMS to:"
msgstr "SMS do:"

#: src/glade/gnocky-addressbook-list-dialog.glade:107
#: src/glade/gnocky-new-sms.glade:107
msgid "*"
msgstr "*"

#: src/glade/gnocky-edit-phonebook.glade:8
#: src/glade/gnocky-edit-phonebook.glade:220
msgid "Edit phonebook entry"
msgstr "Edytuj wpis z książki telefonicznej"

#: src/glade/gnocky-edit-phonebook.glade:88
#: src/glade/gnocky-edit-phonebook.glade:339
msgid "Name:"
msgstr "Nazwa:"

#: src/glade/gnocky-edit-phonebook.glade:116
#: src/glade/gnocky-edit-phonebook.glade:367
msgid "Number:"
msgstr "Numer:"

#: src/glade/gnocky-edit-phonebook.glade:191
msgid "Save changes to phone"
msgstr "Zapisz zmiany do telefonu"

#: src/glade/gnocky-edit-phonebook.glade:259
#: src/glade/gnocky-edit-phonebook.glade:487
msgid "Add phonebook entry"
msgstr "Dodaj wpis do książki telefonicznej"

#: src/glade/gnocky-edit-phonebook.glade:441
msgid "Type:"
msgstr "Typ:"

#: src/glade/gnocky-logos-view.glade:40
msgid "Open file..."
msgstr "Otwórz plik..."

#: src/glade/gnocky-logos-view.glade:56
msgid "Save current picture to disk..."
msgstr "Zapisz obrazek na dysk..."

#: src/glade/gnocky-logos-view.glade:85 src/glade/gnocky-logos-view.glade:86
msgid "Load picture from phone"
msgstr "Wczytaj obrazek z telefonu"

#: src/glade/gnocky-logos-view.glade:103
msgid "Save current picture to phone"
msgstr "Zapisz obrazek do telefonu"

#: src/glade/gnocky-logos-view.glade:134 src/glade/gnocky-logos-view.glade:135
#: src/glade/gnocky-logos-view.glade:174
msgid "Operator logo"
msgstr "Logo operatora"

#: src/glade/gnocky-logos-view.glade:153 src/glade/gnocky-logos-view.glade:154
msgid "Picture message"
msgstr "Wiadomość obrazkowa"

#: src/glade/gnocky-logos-view.glade:173
msgid "Caller id logo"
msgstr "Logo dzwoniącego"

#: src/glade/gnocky-logos-view.glade:193 src/glade/gnocky-logos-view.glade:194
msgid "Startup logo"
msgstr "Logo startowe"

#: src/glade/gnocky-logos-view.glade:226 src/glade/gnocky-logos-view.glade:227
msgid "Pencil"
msgstr "Ołówek"

#: src/glade/gnocky-logos-view.glade:245 src/glade/gnocky-logos-view.glade:246
msgid "Line"
msgstr "Linia"

#: src/glade/gnocky-logos-view.glade:265 src/glade/gnocky-logos-view.glade:266
msgid "Rectangle"
msgstr "Prostokąt"

#: src/glade/gnocky-logos-view.glade:285 src/glade/gnocky-logos-view.glade:286
msgid "Ellipse"
msgstr "Elipsa"

#: src/glade/gnocky-logos-view.glade:305 src/glade/gnocky-logos-view.glade:306
msgid "Fill"
msgstr "Wypełnienie"

#: src/glade/gnocky-phone-view.glade:62
msgid "Manufacturer:"
msgstr "Producent:"

#: src/glade/gnocky-phone-view.glade:91 src/glade/gnocky-phone-view.glade:149
#: src/glade/gnocky-phone-view.glade:207 src/glade/gnocky-phone-view.glade:265
#: src/glade/gnocky-phone-view.glade:479 src/monitor.c:73 src/monitor.c:74
#: src/monitor.c:75 src/monitor.c:76
msgid "Unknown"
msgstr "Nieznany"

#: src/glade/gnocky-phone-view.glade:120
#: src/glade/gnocky-preferences.glade:134
msgid "Model:"
msgstr "Model:"

#: src/glade/gnocky-phone-view.glade:178
msgid "Revision:"
msgstr "Wersja:"

#: src/glade/gnocky-phone-view.glade:236
msgid "IMEI:"
msgstr "IMEI:"

#: src/glade/gnocky-phone-view.glade:302
msgid "Phone Info"
msgstr "Informacje o telefonie"

#: src/glade/gnocky-phone-view.glade:351
msgid "RF Level:"
msgstr "Sygnał sieci:"

#: src/glade/gnocky-phone-view.glade:401
msgid "Battery:"
msgstr "Poziom baterii:"

#: src/glade/gnocky-phone-view.glade:450
msgid "Network:"
msgstr "Sieć:"

#: src/glade/gnocky-phone-view.glade:510
msgid "Status"
msgstr "Status"

#: src/glade/gnocky-phonebook-list-dialog.glade:8
msgid "Phone book"
msgstr "Książka telefoniczna"

#: src/glade/gnocky-phonebook-list-dialog.glade:78
msgid "Please choose number:"
msgstr "Wybierz numer:"

#: src/glade/gnocky-phonebook-view.glade:40 src/glade/gnocky-sms-view.glade:40
msgid "Read from phone"
msgstr "Wczytaj z telefonu"

#: src/glade/gnocky-phonebook-view.glade:58 src/glade/gnocky-sms-view.glade:58
msgid "Write to phone"
msgstr "Zapisz do telefonu"

#: src/glade/gnocky-phonebook-view.glade:89
#, fuzzy
msgid "Save as"
msgstr "Zapisz plik"

#: src/glade/gnocky-phonebook-view.glade:118
#: src/glade/gnocky-sms-view.glade:88
msgid "New"
msgstr "Nowy"

#: src/glade/gnocky-preferences.glade:8
msgid "Gnocky preferences"
msgstr "Preferencje gnocky"

#: src/glade/gnocky-preferences.glade:106
msgid "Port:"
msgstr "Port:"

#: src/glade/gnocky-preferences.glade:162
msgid "Connection:"
msgstr "Połączenie:"

#: src/glade/gnocky-preferences.glade:263
msgid "Gnokii Configuration"
msgstr "Konfiguracja gnokii"

#: src/glade/gnocky-sms-view.glade:239
msgid "Date:"
msgstr "Data:"

#: src/glade/gnocky-sms-view.glade:267
msgid "From:"
msgstr "Od:"

#: src/glade/gnocky.glade:10
msgid "Gnocky"
msgstr "Gnocky"

#: src/glade/gnocky.glade:41
msgid "_File"
msgstr "_Plik"

#: src/glade/gnocky.glade:63
msgid "_Edit"
msgstr "_Edycja"

#: src/glade/gnocky.glade:85
msgid "_Help"
msgstr "P_omoc"

#: src/glade/gnocky.glade:159
msgid "Refresh"
msgstr "Odśwież"

#: src/glade/gnocky.glade:199
msgid "Phone"
msgstr "Telefon"

#: src/glade/gnocky.glade:216
msgid "Phone Book"
msgstr "Książka telefoniczna"

#: src/glade/gnocky.glade:234
msgid "SMS"
msgstr "SMS"

#: src/glade/gnocky.glade:252
msgid "Logos"
msgstr "Logo"

#: src/logosview.c:483
msgid "Open file"
msgstr "Otwórz plik"

#: src/logosview.c:503 src/phonebookview.c:167
msgid "Save file"
msgstr "Zapisz plik"

#: src/logosview.c:599
msgid "Clear"
msgstr "Wyczyść"

#: src/monitor.c:243
msgid "Reading SIM Card phonebook memory"
msgstr "Czytam książkę adresową z karty SIM"

#: src/monitor.c:250
msgid "Reading mobile internal phonebook memory"
msgstr "Czytam książkę adresową z pamięci telefonu"

#: src/monitor.c:293
msgid "Finding free phonebook location..."
msgstr "Szukam wolnego miejsca w książce telefonicznej..."

#: src/monitor.c:297
msgid "Adding phonebook entry..."
msgstr "Dodaję wpis do książki telefonicznej..."

#: src/monitor.c:355
msgid "Writing phonebook to phone..."
msgstr "Zapisuję książkę telefoniczną do telefonu..."

#: src/monitor.c:381
msgid "Reading bitmap from phone..."
msgstr "Wczytuję obrazek z telefonu..."

#: src/monitor.c:398
msgid "Writing bitmap to phone..."
msgstr "Zapisuję obrazek do telefonu..."

#: src/monitor.c:458
msgid "Reading SMS status..."
msgstr "Odczytuję status SMS..."

#: src/monitor.c:464
#, c-format
msgid "Reading SMS list from %s memory..."
msgstr "Odczytuję listę SMSów z pamięci %s..."

#: src/monitor.c:594
msgid "Closing link..."
msgstr "Zamykam połączenie..."

#: src/monitor.c:614
msgid "Phone is not connected!"
msgstr "Telefon nie jest podłączony!"

#: src/monitor.c:670
msgid "Reading phone info..."
msgstr "Wczytuję informacje o telefonie..."

#: src/monitor.c:710
msgid "Cannot read configuration file! Please check ~/.gnokiirc"
msgstr "Nie mogę wczytać pliku konfiguracyjnego! Sprawdź ~/.gnokiirc"

#: src/monitor.c:716
msgid "Cannot load phone!"
msgstr "Nie mogę podłączyć telefonu!"

#: src/monitor.c:721
msgid "Connecting..."
msgstr "Łączę się z telefonem..."

#: src/monitor.c:763
msgid "Connection failed!"
msgstr "Połączenie nieudane!"

#: src/monitor.c:787
msgid "Connection broken!"
msgstr "Połączenie przerwane!"

#: src/phonebookview.c:60 src/phonebookview.c:112 src/smsview.c:164
msgid "Name"
msgstr "Nazwa"

#: src/phonebookview.c:60 src/phonebookview.c:121
msgid "Number"
msgstr "Numer"

#: src/phonebookview.c:103 src/smsview.c:155
msgid "Type"
msgstr "Typ"

#: src/phonebookview.c:177
#, c-format
msgid "Can't open file \"%s\" for writing: %s"
msgstr ""

#: src/phonebookview.c:299
msgid "Do you want to delete this phonebook entry?"
msgstr "Czy chcesz usunąć ten wpis z książki telefonicznej?"

#: src/phonebookview.c:446 src/phonebookview.c:452
#, c-format
msgid "<b>%d</b> used, <b>%d</b> free"
msgstr "<b>%d</b> wykorzystane, <b>%d</b> wolne"

#: src/phonebookview.c:448 src/phonebookview.c:454 src/phoneview.c:54
#: src/phoneview.c:66 src/phoneview.c:79
msgid "N/A"
msgstr "N/D"

#: src/phonebookview.c:457
#, c-format
msgid "SIM card memory: %s. Internal memory: %s"
msgstr "Pamięć karty SIM: %s. Pamięć telefonu: %s"

#: src/smsview.c:79
msgid "Sender"
msgstr "Nadawca"

#: src/smsview.c:88
msgid "Date"
msgstr "Data"

#: src/smsview.c:97
msgid "Memory"
msgstr "Pamięć"

#: src/smsview.c:132
#, c-format
msgid "%d left"
msgstr "%d pozostało"

#: src/smsview.c:398
msgid "Do you want to delete this SMS?"
msgstr "Czy chcesz usunać tę wiadomość SMS?"

#~ msgid "logos_window"
#~ msgstr "Logo"

#~ msgid "Serial"
#~ msgstr "szeregowy"

#~ msgid "IRDA"
#~ msgstr "IrDA"

#~ msgid "Bluetooth"
#~ msgstr "Bluetooth"
