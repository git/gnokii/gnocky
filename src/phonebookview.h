/* $Id$ */

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"

GtkWidget *gnocky_phonebook_view_create();
void gnocky_phonebook_view_destroy(GtkWidget *phonebook_view);
void gnocky_phonebook_view_update(GnockyPhoneMonitor pm);
gchar *gnocky_pb_get_name(gchar *number);
