/* $Id$ */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"
#include "utils.h"

extern GtkWidget *window;
extern GSList *phonebook_list;
extern GMutex *phonebook_mutex;
extern gboolean phonebook_updated;

static GladeXML *phonebook_xml = NULL;
static GtkListStore *store;
static GtkTreeIter iter;
GdkPixbuf *me_pixbuf = NULL;
GdkPixbuf *sm_pixbuf = NULL;

enum {
	COL_TYPE,
	COL_NAME,
	COL_NUMBER,
	COL_POINTER
};

enum {
	GNOCKY_DRAG_TEXT_PLAIN,
	GNOCKY_DRAG_TEXT_X_VCARD
};

static GtkTargetEntry drag_types[] = {
	"text/x-vcard",			 0, GNOCKY_DRAG_TEXT_X_VCARD,
	"text/plain",			 0, GNOCKY_DRAG_TEXT_PLAIN
};

void gnocky_pb_drag_data_get(GtkWidget *widget, GdkDragContext *context, GtkSelectionData *selection_data, guint type, guint time, gpointer user_data)
{
	gn_phonebook_entry *entry;
	char *buffer = NULL;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	if (!g_mutex_trylock (phonebook_mutex)) return;

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(widget));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		gtk_tree_model_get (model, &iter, COL_POINTER, &entry, -1);
		switch (type) {
			case GNOCKY_DRAG_TEXT_X_VCARD:
				buffer = g_strdup_printf ("BEGIN:VCARD\nVERSION:3.0\nFN:%s\nTEL;TYPE=PREF,VOICE:%s\nEND:VCARD\n\n", entry->name, entry->number);
				break;
			default:
				buffer = g_strdup_printf ("%s: %s\n%s: %s\n\n", _("Name"), entry->name, _("Number"), entry->number);
				break;
		}

		gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING, 8, buffer, strlen(buffer));
		g_free (buffer);
	}

	g_mutex_unlock(phonebook_mutex);
}

static gint gnocky_pb_sort_type_column(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer user_data)
{
	gn_phonebook_entry *ena, *enb;
	gint ret;

	gtk_tree_model_get (model, a, COL_POINTER, &ena, -1);
	gtk_tree_model_get (model, b, COL_POINTER, &enb, -1);

	/* sort order for memory types is arbitrary */
	ret = ena->memory_type - enb->memory_type;
	if (!ret)
		ret = ena->location - enb->location;

	return ret;
}

static GtkListStore *gnocky_pb_create_model()
{
	return gtk_list_store_new(4, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);
}


static void gnocky_pb_add_columns(GtkTreeView *treeview)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model (treeview);
	
	renderer = gtk_cell_renderer_pixbuf_new();
	column = gtk_tree_view_column_new_with_attributes (_("Type"),
						     renderer,
						     "pixbuf",
						     COL_TYPE,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_TYPE);
	gtk_tree_view_append_column (treeview, column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "text",
						     COL_NAME,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_NAME);
	gtk_tree_view_append_column (treeview, column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Number"),
						     renderer,
						     "text",
						     COL_NUMBER,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_NUMBER);
	gtk_tree_view_append_column (treeview, column);

	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE(model), COL_TYPE, gnocky_pb_sort_type_column, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(model), COL_NAME, GTK_SORT_ASCENDING);
}

void gnocky_pb_load_from_phone_clicked(GtkWidget *widget, gpointer data)
{
	gnocky_event_push(GNOCKY_EVENT_READ_PHONEBOOK, NULL);
}

void gnocky_pb_save_to_phone_clicked(GtkWidget *widget, gpointer data)
{
	gnocky_event_push(GNOCKY_EVENT_WRITE_PHONEBOOK, NULL);
}

static void gnocky_pb_save_entry(gpointer data, gpointer user_data)
{
	gchar *location;
	FILE *file = user_data;
	gn_phonebook_entry *entry = data;

	if (entry->memory_type == GN_MT_SM)
		location = g_strdup_printf("SM%d", entry->location);
	else
		location = g_strdup_printf("ME%d", entry->location);
	gn_phonebook2vcard(file, entry, location);
	g_free(location);
}

void gnocky_pb_save_to_disk_clicked(GtkWidget * button, gpointer user_data)
{
	GtkWidget *file_selector;
	gint response;
	const gchar *filename;
	gchar *message;
	FILE *file;

	if (!g_mutex_trylock(phonebook_mutex)) return;

	file_selector = gtk_file_selection_new(_("Save file"));
	gtk_window_set_transient_for (GTK_WINDOW (file_selector), GTK_WINDOW (window));
	response = gtk_dialog_run(GTK_DIALOG(file_selector));

	if (response == GTK_RESPONSE_OK) {
		filename = gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selector));
		file = fopen(filename, "w");
		if (file) {
			g_slist_foreach(phonebook_list, gnocky_pb_save_entry, file);
			fclose(file);
		} else {
			message = g_strdup_printf(_("Can't open file \"%s\" for writing: %s"), filename, g_strerror(errno));
			gnocky_show_error(message);
			g_free(message);
		}
	}
	gtk_widget_destroy(file_selector);

	g_mutex_unlock(phonebook_mutex);
}

static void show_edit_pb_dialog(gn_phonebook_entry *en, gint type)
{
	GladeXML *edit_xml;
	GtkWidget *edit;
	GtkWidget *entry;
	GtkWidget *memory;
	gchar *name, *number;
	gchar *dialog_id;
	gint res;
	GtkTreeModel *model;
	enum {COL_TYPE, COL_MEMORY_TYPE};

	if (type == GNOCKY_EVENT_WRITE_PHONEBOOK_ENTRY) {
		dialog_id = "edit_phonebook_dialog";
	} else {
		dialog_id = "add_phonebook_entry";
	}
	edit_xml = create_gladexml("gnocky-edit-phonebook.glade", dialog_id);
	if (!edit_xml)
		g_error("Cannot create phonebook edit view!\n");
	
	name = g_locale_to_utf8(en->name, -1, NULL, NULL, NULL);
	entry = glade_xml_get_widget(edit_xml, "name_entry");
	gtk_entry_set_text(GTK_ENTRY(entry), name);
	g_free(name);
	
	number = g_locale_to_utf8(en->number, -1, NULL, NULL, NULL);
	entry = glade_xml_get_widget(edit_xml, "number_entry");
	gtk_entry_set_text(GTK_ENTRY(entry), number);
	g_free(number);

	if (type == GNOCKY_EVENT_ADD_PHONEBOOK_ENTRY) {
		GtkCellRenderer *renderer;
		GtkTreeStore *store;
		GtkTreeIter iter;

		memory = glade_xml_get_widget (edit_xml, "type_combo");
		renderer = gtk_cell_renderer_pixbuf_new ();
		gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (memory), renderer, TRUE);
		gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (memory), renderer, "pixbuf", COL_TYPE, NULL);

		store = gtk_tree_store_new (2, GDK_TYPE_PIXBUF, G_TYPE_INT);

		/* TODO only enable memories supported by this phone and with at least one free location */
		gtk_tree_store_append (store, &iter, NULL);
		gtk_tree_store_set (store, &iter, COL_TYPE, me_pixbuf, COL_MEMORY_TYPE, GN_MT_ME, -1);

		gtk_tree_store_append (store, &iter, NULL);
		gtk_tree_store_set (store, &iter, COL_TYPE, sm_pixbuf, COL_MEMORY_TYPE, GN_MT_SM, -1);

		gtk_combo_box_set_model (GTK_COMBO_BOX(memory), GTK_TREE_MODEL(store));
		g_object_unref (store);

		gtk_combo_box_set_active (GTK_COMBO_BOX(memory), 0);
	}

	edit = glade_xml_get_widget(edit_xml, dialog_id);
	gtk_window_set_transient_for (GTK_WINDOW (edit), GTK_WINDOW (window));
	res = gtk_dialog_run(GTK_DIALOG(edit));
	if (res == GTK_RESPONSE_OK) {
		GtkWidget *widget;
		
		entry = glade_xml_get_widget(edit_xml, "name_entry");
		name = g_locale_from_utf8(gtk_entry_get_text(GTK_ENTRY(entry)), -1, NULL, NULL, NULL);
		snprintf(en->name, sizeof(en->name), "%s", name);
		g_free(name);
		
		entry = glade_xml_get_widget(edit_xml, "number_entry");
		number = g_locale_from_utf8(gtk_entry_get_text(GTK_ENTRY(entry)), -1, NULL, NULL, NULL);
		snprintf(en->number, sizeof(en->number), "%s", number);
		g_free(number);
		
		if (type == GNOCKY_EVENT_WRITE_PHONEBOOK_ENTRY) {
			widget = glade_xml_get_widget(edit_xml, "save_changes_check");
			if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
				gnocky_event_push(type, en);
			else 
				phonebook_updated = TRUE;
		} else {
			if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX(memory), &iter)) {
				model = gtk_combo_box_get_model (GTK_COMBO_BOX(memory));
				gtk_tree_model_get (model, &iter, COL_MEMORY_TYPE, &en->memory_type, -1);
				/* unconditionally add the new phonebook entry since it needs to find an empty location */
				gnocky_event_push(type, en);
			}
		}
	} else {
		if (type == GNOCKY_EVENT_ADD_PHONEBOOK_ENTRY) {
			g_free(en);
		}
	}
	gtk_widget_destroy(edit);
	g_object_unref(G_OBJECT(edit_xml));
}

gboolean pb_pressed(GtkWidget *widget, GdkEventKey *ev, gpointer user_data)
{
	if (ev->keyval == GDK_Delete) {
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
		GtkTreePath *treepath = NULL;
		GtkTreeIter iter;
		GtkWidget *dialog;
		gint res;
		gn_phonebook_entry *en;
		
		gtk_tree_view_get_cursor (GTK_TREE_VIEW(widget), &treepath, NULL);
		if (!treepath) return FALSE;
		gtk_tree_model_get_iter(model, &iter, treepath);
		gtk_tree_model_get(model, &iter, 3, &en, -1);
		
		dialog = gtk_message_dialog_new(GTK_WINDOW(window), GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, 
                                             GTK_BUTTONS_OK_CANCEL,
					     _("Do you want to delete this phonebook entry?"));

		res = gtk_dialog_run(GTK_DIALOG(dialog));
		if (res == GTK_RESPONSE_OK) {
			gnocky_event_push(GNOCKY_EVENT_DELETE_PHONEBOOK_ENTRY, en);
		}
		gtk_tree_path_free(treepath);
		
		gtk_widget_destroy(dialog);
	}
	return FALSE;
}

gboolean pb_clicked(GtkWidget *widget, GdkEventButton *ev, gpointer user_data)
{
	if (ev->type == GDK_2BUTTON_PRESS && ev->button == 1) {
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
		GtkTreeViewColumn *treevc = NULL;
		GtkTreePath *treepath = NULL;
		GtkTreeIter iter;
		gn_phonebook_entry *en;
		
		if (!gtk_tree_view_get_path_at_pos
		    (GTK_TREE_VIEW(widget), ev->x, ev->y, &treepath, &treevc, NULL, NULL))
			return FALSE;
		gtk_tree_model_get_iter(model, &iter, treepath);
		gtk_tree_path_free(treepath);
		gtk_tree_model_get(model, &iter, 3, &en, -1);
		show_edit_pb_dialog(en, GNOCKY_EVENT_WRITE_PHONEBOOK_ENTRY);
		g_print("button_clicked 2 times\n");
	}
	return FALSE;
}

gboolean gnocky_pb_new_entry_clicked(GtkWidget *widget, GdkEventButton *ev, gpointer user_data)
{
	gn_phonebook_entry *en;

	en = g_new0(gn_phonebook_entry, 1);
	if (en)
		show_edit_pb_dialog(en, GNOCKY_EVENT_ADD_PHONEBOOK_ENTRY);

	return FALSE;
}

void gnocky_phonebook_view_destroy(GtkWidget *phonebook_view)
{
	if (sm_pixbuf)
		gdk_pixbuf_unref(sm_pixbuf);
	if (me_pixbuf)
		gdk_pixbuf_unref(me_pixbuf);
	if (phonebook_view)
		gtk_widget_destroy(phonebook_view);
	if (phonebook_xml)
		g_object_unref(phonebook_xml);
}

GtkWidget *gnocky_phonebook_view_create()
{
	GtkWidget *treeview;
	GtkWidget *pbv;

	phonebook_xml = create_gladexml("gnocky-phonebook-view.glade", "phonebook_view");

	if (!phonebook_xml)
		g_error("Cannot create phone view!\n");

	glade_xml_signal_autoconnect(phonebook_xml);

	pbv = glade_xml_get_widget(phonebook_xml, "phonebook_view");

	if (pbv == NULL)
		g_error("Wrong glade data!\n");
	
	store = gnocky_pb_create_model();
	treeview = glade_xml_get_widget(phonebook_xml, "phonebook_tree");
	gtk_tree_view_set_model(GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(store));
	g_object_unref(store);
	
	gnocky_pb_add_columns(GTK_TREE_VIEW (treeview));
	gtk_drag_source_set(treeview, GDK_BUTTON1_MASK, drag_types, sizeof(drag_types)/sizeof(drag_types[0]), GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_ASK);

	me_pixbuf = create_pixbuf("me.png");
	sm_pixbuf = create_pixbuf("sm.png");
	
	gtk_widget_show_all(pbv);
	
	return pbv;
}

void gnocky_phonebook_view_update(GnockyPhoneMonitor pm)
{
	GtkWidget *widget;
	
	if (g_mutex_trylock(phonebook_mutex)) {
		if (phonebook_updated) {
			GSList *tmp = phonebook_list;
			gtk_widget_set_sensitive(glade_xml_get_widget(phonebook_xml, "pb_save_to_disk"), tmp != NULL);
			gtk_widget_set_sensitive(glade_xml_get_widget(phonebook_xml, "pb_save_to_phone"), tmp != NULL);
			gtk_list_store_clear(store);
			while (tmp) {
				gn_phonebook_entry *en = (gn_phonebook_entry *) tmp->data;
				gchar *number_string = g_strdup("");
				gchar *name = g_locale_to_utf8(en->name, -1, NULL, NULL, NULL);
				
				if (en->subentries_count) {
					gint i;
					gchar *tmp;
					
					for (i = 0;i<en->subentries_count;i++) {
						tmp = g_strdup_printf("%s %s", number_string, en->subentries[i].data.number);
						g_free(number_string);
						number_string = tmp;
					}
				} else {
					g_free(number_string);
					number_string = g_strdup(en->number);
				}
				
				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, COL_NAME, name, COL_NUMBER, number_string, COL_POINTER, en, -1);
				
				g_free(name);
				g_free(number_string);
				
				switch (en->memory_type) {
					case GN_MT_ME:
						gtk_list_store_set (store, &iter, COL_TYPE, me_pixbuf, -1);
						break;
					case GN_MT_SM:
						gtk_list_store_set (store, &iter, COL_TYPE, sm_pixbuf, -1);
						break;
					default:
						break;
				}
				tmp = tmp->next;
			}
			phonebook_updated = FALSE;
		}
		g_mutex_unlock(phonebook_mutex);
	}
	widget = glade_xml_get_widget(phonebook_xml, "memory_status_label");
	if (widget) {
		gchar *sm_memory_info;
		gchar *me_memory_info;
		gchar *memory_info; 
		
		if (pm.ms_sm_used != -1) {
			sm_memory_info = g_strdup_printf(_("<b>%d</b> used, <b>%d</b> free"), pm.ms_sm_used, pm.ms_sm_free);
		} else {
			sm_memory_info = g_strdup(_("N/A"));
		}
		
		if (pm.ms_me_used != -1) {
			me_memory_info = g_strdup_printf(_("<b>%d</b> used, <b>%d</b> free"), pm.ms_me_used, pm.ms_me_free);
		} else {
			me_memory_info = g_strdup(_("N/A"));
		}
		
		memory_info = g_strdup_printf(_("SIM card memory: %s. Internal memory: %s"), sm_memory_info, me_memory_info);
		gtk_label_set_markup(GTK_LABEL(widget), memory_info);
		
		g_free(sm_memory_info);
		g_free(me_memory_info);
		g_free(memory_info);
	}
}

gchar *gnocky_pb_get_name(gchar *number)
{
	GSList *tmp;
	if (!number || !number[0])
		return NULL;
	
	if (g_mutex_trylock(phonebook_mutex)) {
		gchar *name, *tmpnumber;
		tmp = phonebook_list;
		
		if (number[0] == '+')
			number += 3;
		
		while (tmp) {
			gn_phonebook_entry *en = (gn_phonebook_entry *) tmp->data;
			tmpnumber = en->number;
			
			if (en->number[0] == '+') 
				tmpnumber += 3;

			if (!g_strcasecmp(tmpnumber, number)) {
				name = g_strdup(en->name);
				g_mutex_unlock(phonebook_mutex);
				return name;
			}
			tmp = tmp->next;
		}
		g_mutex_unlock(phonebook_mutex);
	}
	return NULL;
}
