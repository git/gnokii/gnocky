/* $Id$ */

#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"
#include "utils.h"
#include "logosview.h"


typedef struct _point {
	gint x;
	gint y;
} Point;

//extern GMutex *pm.bitmap_mutex;
extern GnockyPhoneMonitor pm;

static GladeXML *logos_xml;

static GtkWidget *da = NULL;
static GtkWidget *dapreview = NULL;
static GdkPixmap *pixmap = NULL;
static GdkPixmap *preview = NULL;
static GdkGC *gc;
static GdkGC *grid;
static GdkGC *pgc;

static gn_bmp buffer;
static gn_bmp_types current_bmp_type = GN_BMP_OperatorLogo;
static gint dm_set = DM_DEFAULT;
static gint dm_current = DM_POINT;

static gint dm_x = 0, dm_y = 0;
static gint dm_last_x = 0, dm_last_y = 0;
static gboolean initialized = FALSE;

/* prototypes of internal drawing routines */
void clearpixel_bool(gint x, gint y);
void putpixel(gint _x, gint _y, gboolean clear);
void putpixel_all(gint x, gint y, gboolean clear);
void clearpixel_bool(int x, int y);
void line_bool(int x0, int y0, int x1, int y1);
void line(int x0, int y0, int x1, int y1, int clear);
void rect_bool(int x0, int y0, int x1, int y1);
void rect(int x0, int y0, int x1, int y1, int clear);
void ellipse_bool(int cx, int cy, int xr, int yr);
void ellipse (int cx, int cy, int xr, int yr, int clear);
void resize(gn_bmp_types type);
void fill(int x, int y, int clear);

void gnocky_copy_bmp_to_screen(gn_bmp *bmp)
{
	gint x,y;
	gboolean clear;
	GdkRectangle update_rect;
	g_print("copy to screen %d x %d\n", bmp->width, bmp->height);
	update_rect.width = TILE_SIZE - 1;
	update_rect.height = TILE_SIZE - 1;
	
	for (x = 0;x<bmp->width;x++) {
		for (y = 0;y<bmp->height;y++) {
			if (gn_bmp_point(bmp, x, y))
				clear = 0;
			else
				clear = 1;

			gdk_draw_point(preview, (clear) ? pgc : dapreview->style->black_gc, x, y);

			update_rect.x = x * TILE_SIZE + 1;
			update_rect.y = y * TILE_SIZE + 1;

			gdk_draw_rectangle(pixmap,
			   (clear) ? gc : da->style->black_gc,
			   TRUE,
			   update_rect.x, update_rect.y,
			   update_rect.width, update_rect.height);
		}
	}

	update_rect.x = 0;
	update_rect.y = 0;

	update_rect.width = da->allocation.width;
	update_rect.height = da->allocation.height;
	update_rect.width = bmp->width  * TILE_SIZE;
	update_rect.height = bmp->height * TILE_SIZE;	
	
	gdk_window_invalidate_rect(da->window, &update_rect, FALSE);

	update_rect.width = bmp->width;
	update_rect.height = bmp->height;	

	gdk_window_invalidate_rect(dapreview->window, &update_rect, FALSE);
}
static
void change_size(gint xsize, gint ysize)
{
	g_print("%d %d\n", xsize, ysize);
	
	gtk_widget_set_size_request(da, TILE_SIZE * xsize + 1,
				    TILE_SIZE * ysize + 1);
	gtk_widget_set_size_request(dapreview, xsize, ysize);

	if (pixmap)
		g_object_unref(pixmap);

	if (preview)
		g_object_unref(preview);

	pixmap = gdk_pixmap_new(da->window,
				xsize * TILE_SIZE + 1,
				ysize * TILE_SIZE + 1, -1);

	gdk_draw_rectangle(pixmap,
			   gc,
			   TRUE,
			   0, 0,
			   xsize * TILE_SIZE, ysize * TILE_SIZE);
	
	preview = gdk_pixmap_new(dapreview->window,
				 xsize,
				 ysize, -1);

	gdk_draw_rectangle(preview,
			   pgc,
			   TRUE,
			   0, 0,
			   xsize, ysize);

}

static gboolean
scribble_button_release_event(GtkWidget * widget,
			    GdkEventButton * event, gpointer data)
{
	if (pixmap == NULL)
		return FALSE;	/* paranoia check, in case we haven't gotten a configure event */
	g_print("release\n");
	
	if (dm_set == DM_FILL) {
		gint x, y;
		gboolean clear = (event->button == 3);

		x = (gint)event->x / TILE_SIZE;
		y = (gint)event->y / TILE_SIZE;
		
		fill(x, y, clear);
	} else {
		dm_current = DM_DEFAULT;
	}
	return TRUE;
}


static gboolean
scribble_button_press_event(GtkWidget * widget,
			    GdkEventButton * event, gpointer data)
{
	gint x,y;
	if (pixmap == NULL)
		return FALSE;	/* paranoia check, in case we haven't gotten a configure event */
	
	x = (gint)event->x / TILE_SIZE;
	y = (gint)event->y / TILE_SIZE;

	if ((event->button == 1 || event->button == 3)) {
	    gboolean clear = (event->button == 3) ? TRUE : FALSE;
	    switch (dm_set) {
		case DM_LINE:
		    if (dm_set != dm_current) {
		    	dm_current = dm_set;
		    	dm_last_x = dm_x = x;
		    	dm_last_y = dm_y = y;
		    	memcpy(&buffer, &pm.bitmap, sizeof(buffer));
		    } else {
		    	dm_current = DM_DEFAULT;
		    	line(dm_x, dm_y, x, y, clear);
		    }
		    break;
		case DM_RECT:
		    if (dm_set != dm_current) {
		    	dm_current = dm_set;
		    	dm_last_x = dm_x = x;
		    	dm_last_y = dm_y = y;
		    	memcpy(&buffer, &pm.bitmap, sizeof(buffer));
		    } else {
		    	dm_current = DM_DEFAULT;
		    	rect(dm_x, dm_y, x, y, clear);
		    }
		    break;
		case DM_ELLIPSE:
		    if (dm_set != dm_current) {
		    	dm_current = dm_set;
		    	dm_last_x = dm_x = x;
		    	dm_last_y = dm_y = y;
		    	memcpy(&buffer, &pm.bitmap, sizeof(buffer));
		    } else {
		    	dm_current = DM_DEFAULT;
		    	ellipse(x, y, abs(x - dm_x), abs(y - dm_y), clear);
		    }
		    break;
		case DM_FILL:
			break;
		default:
		    putpixel_all(x, y, clear);
		    break;
	    }
	}
	
	return TRUE;
}

static gboolean
scribble_motion_notify_event(GtkWidget * widget,
			     GdkEventMotion * event, gpointer data)
{
	gint x, y;
	GdkModifierType state;
	if (pixmap == NULL)
		return FALSE;

	gdk_window_get_pointer(event->window, &x, &y, &state);
	
	x = (gint)event->x / TILE_SIZE;
	y = (gint)event->y / TILE_SIZE;
	
	if (state & GDK_BUTTON1_MASK || state & GDK_BUTTON3_MASK) {
	    gboolean clear = (state & GDK_BUTTON3_MASK) ? TRUE : FALSE;
		switch (dm_current) {
		    case DM_LINE:
			line_bool(dm_x, dm_y, dm_last_x, dm_last_y);
			line(dm_x, dm_y, x, y, clear);
			dm_set = DM_LINE;
			dm_last_x = x;
			dm_last_y = y;
			break;
		    case DM_RECT:
			rect_bool(dm_x, dm_y, dm_last_x, dm_last_y);
			rect(dm_x, dm_y, x, y, clear);
			dm_set = DM_RECT;
			dm_last_x = x;
			dm_last_y = y;
			break;
		    case DM_ELLIPSE:
			ellipse_bool(dm_last_x, dm_last_y, abs(dm_x - dm_last_x), abs(dm_y - dm_last_y));
		    	ellipse(x, y, abs(x - dm_x), abs(y - dm_y), clear);
			dm_set = DM_ELLIPSE;
			dm_last_x = x;
			dm_last_y = y;
			break;
		    case DM_FILL:
		    	break;
		    default:
			putpixel_all(x, y, clear);
			break;

		}
	}
	return TRUE;
}

/* Create a new pixmap of the appropriate size to store our scribbles */
static gboolean
scribble_configure_event(GtkWidget * da,
			 GdkEventConfigure * event, gpointer data)
{
	GdkColor color;

	if (!gc) {
		gc = gdk_gc_new(da->window);
		gdk_color_parse("#00ff01", &color);
		gdk_gc_set_rgb_fg_color(gc, &color);
	}
	if (!grid) {
		grid = gdk_gc_new(da->window);
		gdk_color_parse("#808080", &color);
		gdk_gc_set_rgb_fg_color(grid, &color);
	}
	
	if (pixmap)
		return TRUE;
	
	pixmap = gdk_pixmap_new(da->window,
				da->allocation.width,
				da->allocation.height, -1);

	gdk_draw_rectangle(pixmap,
			   gc,
			   TRUE,
			   0, 0,
			   da->allocation.width, da->allocation.height);

	return TRUE;
}

static gboolean
preview_configure_event(GtkWidget * da,
			GdkEventConfigure * event, gpointer data)
{
	GdkColor color;

	if (!pgc) {
		pgc = gdk_gc_new(da->window);
		gdk_color_parse("#7cc17c", &color);
		gdk_gc_set_rgb_fg_color(pgc, &color);
	}

	if (preview)
		return TRUE;

	preview = gdk_pixmap_new(da->window,
				 da->allocation.width,
				 da->allocation.height, -1);

	gdk_draw_rectangle(preview,
			   pgc,
			   TRUE,
			   0, 0,
			   da->allocation.width, da->allocation.height);

	return TRUE;
}

/* Redraw the screen from the pixmap */
static gboolean
scribble_expose_event(GtkWidget * da,
		      GdkEventExpose * event, gpointer data)
{
	gint i;
	
	/* FIXME */
	if (!initialized) {
		resize(current_bmp_type);
		initialized = TRUE;
	}
	gdk_draw_drawable(da->window,
			  gc,
			  pixmap,
			  event->area.x, event->area.y,
			  event->area.x, event->area.y,
			  event->area.width, event->area.height);

	for (i = 0; i < da->allocation.width; i += TILE_SIZE)
		gdk_draw_line(da->window, grid, i, 0, i,
			      da->allocation.height);
	for (i = 0; i < da->allocation.height; i += TILE_SIZE)
		gdk_draw_line(da->window, grid, 0, i, da->allocation.width,
			      i);
	return FALSE;
}

static gboolean
preview_expose_event(GtkWidget * da, GdkEventExpose * event, gpointer data)
{
	gdk_draw_drawable(da->window,
			  pgc,
			  preview,
			  event->area.x, event->area.y,
			  event->area.x, event->area.y,
			  event->area.width, event->area.height);

	return FALSE;
}
/* button callbacks */

void resize(gn_bmp_types type)
{
	gn_bmp_resize(&pm.bitmap, type, &pm.info);
	change_size(pm.bitmap.width, pm.bitmap.height);
	memcpy(&buffer, &pm.bitmap, sizeof(pm.bitmap));
	gnocky_copy_bmp_to_screen(&pm.bitmap);
	current_bmp_type = type;
}

void operator_logo_radio_toggled(GtkWidget *button, gpointer user_data)
{
	resize(GN_BMP_OperatorLogo);
}

void picture_message_radio_toggled(GtkWidget *button, gpointer user_data)
{
	resize(GN_BMP_PictureMessage);
}

void caller_id_radio_toggled(GtkWidget *button, gpointer user_data)
{
	resize(GN_BMP_CallerLogo);
}

void startup_logo_radio_toggled(GtkWidget *button, gpointer user_data)
{
	resize(GN_BMP_StartupLogo);
}

void change_dm(gint dm)
{
	dm_set = dm;
	dm_current = DM_DEFAULT;
}

void pencil_radio_toggled(GtkWidget * button, gpointer user_data)
{
	change_dm(DM_POINT);
}

void line_radio_toggled(GtkWidget * button, gpointer user_data)
{
	change_dm(DM_LINE);
}

void rect_radio_toggled(GtkWidget * button, gpointer user_data)
{
	change_dm(DM_RECT);
}

void ellipse_radio_toggled(GtkWidget * button, gpointer user_data)
{
	change_dm(DM_ELLIPSE);
}

void fill_radio_toggled(GtkWidget * button, gpointer user_data)
{
	change_dm(DM_FILL);
	dm_current = DM_FILL;
}

void download_button_clicked(GtkWidget * button, gpointer user_data)
{
	gnocky_event_push(GNOCKY_EVENT_READ_BITMAP, (gpointer)pm.bitmap.type);
}

void upload_button_clicked(GtkWidget * button, gpointer user_data)
{
	gnocky_event_push(GNOCKY_EVENT_WRITE_BITMAP, (gpointer)pm.bitmap.type);
}

static
void clear_button_clicked(GtkWidget * button, gpointer user_data)
{
	GdkRectangle update_rect;

	update_rect.x = 0;
	update_rect.y = 0;
	update_rect.width = da->allocation.width;
	update_rect.height = da->allocation.height;

	gdk_draw_rectangle(pixmap,
			   gc,
			   TRUE,
			   0, 0,
			   da->allocation.width, da->allocation.height);

	gdk_window_invalidate_rect(da->window, &update_rect, FALSE);

	update_rect.x = 0;
	update_rect.y = 0;
	update_rect.width = dapreview->allocation.width;
	update_rect.height = dapreview->allocation.height;

	gdk_draw_rectangle(preview,
			   pgc,
			   TRUE,
			   0, 0,
			   dapreview->allocation.width,
			   dapreview->allocation.height);

	gdk_window_invalidate_rect(dapreview->window, &update_rect, FALSE);
	gn_bmp_clear(&pm.bitmap);
	memcpy(&buffer, &pm.bitmap, sizeof(pm.bitmap));
}


void open_button_clicked(GtkWidget * button, gpointer user_data)
{
	GtkWidget *file_selector;
	gint response;
	gchar *filename = NULL;
	
	file_selector = gtk_file_selection_new(_("Open file"));
	response = gtk_dialog_run(GTK_DIALOG(file_selector));
	
	if (response == GTK_RESPONSE_OK) {
		filename = (gchar *)gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selector));
		gn_bmp_clear(&pm.bitmap);
		gn_file_bitmap_read(filename, &pm.bitmap, &pm.info);
		change_size(pm.bitmap.width, pm.bitmap.height);
		gnocky_copy_bmp_to_screen(&pm.bitmap);
	}
    	gtk_widget_destroy(file_selector);
	resize(current_bmp_type);
}

void save_button_clicked(GtkWidget * button, gpointer user_data)
{
	GtkWidget *file_selector;
	gint response;
	gchar *filename = NULL;

	file_selector = gtk_file_selection_new(_("Save file"));
	response = gtk_dialog_run(GTK_DIALOG(file_selector));

	if (response == GTK_RESPONSE_OK) {
		filename = (gchar *)gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selector));
		gn_file_bitmap_save(filename, &pm.bitmap, &pm.info);
	}

	gtk_widget_destroy(file_selector);
}

void gnocky_logos_view_destroy(GtkWidget *logos_view)
{
	if (logos_view)
		gtk_widget_destroy(logos_view);
	if (logos_xml)
		g_object_unref(logos_xml);
}

GtkWidget *gnocky_logos_view_create()
{
	GtkWidget *lv;
	GtkWidget *hbox;
	GtkWidget *left_vbox;
	GtkWidget *frame;
	GtkWidget *right_vbox;
	GtkWidget *button;
	
	logos_xml = create_gladexml("gnocky-logos-view.glade", "logos_view");

	if (!logos_xml)
		g_error("Cannot create logos view!\n");

	glade_xml_signal_autoconnect(logos_xml);

	lv = glade_xml_get_widget(logos_xml, "logos_view");

	if (lv == NULL)
		g_error("Wrong glade data!\n");
	
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(lv), hbox, TRUE, TRUE, 0);

	left_vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), left_vbox, FALSE, FALSE, 0);

	frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

	dapreview = gtk_drawing_area_new();
	gtk_widget_set_size_request(dapreview, DEFAULT_XSIZE,
				    DEFAULT_YSIZE);
	gtk_container_add(GTK_CONTAINER(frame), dapreview);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 4);
	gtk_box_pack_start(GTK_BOX(left_vbox), frame, FALSE, FALSE, 0);

	frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

	da = gtk_drawing_area_new();
	gtk_widget_set_size_request(da, TILE_SIZE * DEFAULT_XSIZE + 1,
				    TILE_SIZE * DEFAULT_YSIZE + 1);

	right_vbox = gtk_vbox_new(FALSE, 8);
	gtk_box_pack_start(GTK_BOX(hbox), right_vbox, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frame), da);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 4);
	gtk_box_pack_start(GTK_BOX(right_vbox), frame, FALSE, FALSE, 0);

	/* Signals used to handle backing pixmap */

	g_signal_connect(da, "expose_event",
			 G_CALLBACK(scribble_expose_event), NULL);
	g_signal_connect(da, "configure_event",
			 G_CALLBACK(scribble_configure_event), NULL);

	g_signal_connect(dapreview, "configure_event",
			 G_CALLBACK(preview_configure_event), NULL);
	g_signal_connect(dapreview, "expose_event",
			 G_CALLBACK(preview_expose_event), NULL);
	
	/* Event signals */
	g_signal_connect(da, "motion_notify_event",
			 G_CALLBACK(scribble_motion_notify_event), NULL);
	g_signal_connect(da, "button_press_event",
			 G_CALLBACK(scribble_button_press_event), NULL);
	g_signal_connect(da, "button_release_event",
			 G_CALLBACK(scribble_button_release_event), NULL);
	
	gtk_widget_set_events(da, gtk_widget_get_events(da)
			      | GDK_LEAVE_NOTIFY_MASK
			      | GDK_BUTTON_PRESS_MASK
			      | GDK_BUTTON_RELEASE_MASK
			      | GDK_POINTER_MOTION_MASK
			      | GDK_POINTER_MOTION_HINT_MASK);

	button = gtk_button_new_with_label(_("Clear"));
	g_signal_connect(button, "clicked",
			 G_CALLBACK(clear_button_clicked), NULL);
	gtk_box_pack_end(GTK_BOX(left_vbox), button, FALSE, FALSE, 0);
			      
	gtk_widget_show_all(lv);

	return lv;
}

void gnocky_logos_view_update(GnockyPhoneMonitor pm)
{
	
}

/* drawing routines */
void clearpixel_bool(gint x, gint y)
{
    if (x < 0 || y < 0) 
	return;
    if (x > pm.bitmap.width || y > pm.bitmap.height) 
	return;

	putpixel_all(x, y, !gn_bmp_point(&buffer, x, y));
}

void putpixel(gint _x, gint _y, gboolean clear)
{
	GdkRectangle update_rect;
	
	update_rect.x = _x * TILE_SIZE + 1;
	update_rect.y = _y * TILE_SIZE + 1;
	update_rect.width = TILE_SIZE - 1;
	update_rect.height = TILE_SIZE - 1;

	gdk_draw_rectangle(da->window,
			   (clear) ? gc : da->style->black_gc,
			   TRUE,
			   update_rect.x, update_rect.y,
			   update_rect.width, update_rect.height);
	gdk_draw_rectangle(pixmap,
			   (clear) ? gc : da->style->black_gc,
			   TRUE,
			   update_rect.x, update_rect.y,
			   update_rect.width, update_rect.height);
	gdk_draw_point(dapreview->window,
		       (clear) ? pgc : dapreview->style->black_gc, _x, _y);	
	gdk_draw_point(preview,
		       (clear) ? pgc : dapreview->style->black_gc, _x, _y);	
}

void putpixel_all(gint x, gint y, gboolean clear)
{
//	g_print("pp %d %d\n", x, y);
	
	if (x < 0 || y < 0) 
	    return;
    
	if (x > pm.bitmap.width || y > pm.bitmap.height) 
	    return;

	putpixel(x,y, clear);

	if (clear)
		gn_bmp_point_clear(&pm.bitmap, x, y);
	else
		gn_bmp_point_set(&pm.bitmap, x, y);

}

gboolean is_point_set(gint x, gint y, gboolean clear)
{
	if (x < 0 || y < 0) 
	    return TRUE;
    
	if (x > pm.bitmap.width || y > pm.bitmap.height) 
	    return TRUE;
    	
	return gn_bmp_point(&pm.bitmap, x, y) != clear;
}

void line_bool(int x0, int y0, int x1, int y1)
{
	int dy, dx;
	float t = (float) 0.5;

	dy = y1 - y0;
	dx = x1 - x0;
	clearpixel_bool(x0, y0);
        if (abs(dx) > abs(dy)) {
            float m = (float) dy / (float) dx;
            t += y0;
            dx = (dx < 0) ? -1 : 1;
            m *= dx;
            while (x0 != x1) {
                x0 += dx;
                t += m;
		clearpixel_bool(x0, (int)t);
            }
	} else {
            float m = (float) dx / (float) dy;
            t += x0;
            dy = (dy < 0) ? -1 : 1;
            m *= dy;
            while (y0 != y1) {
                y0 += dy;
                t += m;
		clearpixel_bool((int) t, y0);
	    }
	}
}

void line(int x0, int y0, int x1, int y1, int clear)
{
    int dy, dx;
    float t = (float) 0.5;

    dy = y1 - y0;
    dx = x1 - x0;

    putpixel_all((gdouble) x0, (gdouble)y0, clear);

    if (abs(dx) > abs(dy)) {
	float m = (float) dy / (float) dx;
        t += y0;
        dx = (dx < 0) ? -1 : 1;
        m *= dx;
        while (x0 != x1) {
            x0 += dx;
            t += m;
	    putpixel_all(x0, (int) t, clear);
        }
    } else {
        float m = (float) dx / (float) dy;
        t += x0;
        dy = (dy < 0) ? -1 : 1;
        m *= dy;
	while (y0 != y1) {
            y0 += dy;
            t += m;
	    putpixel_all((int) t, y0, clear);
	}
    }
}

void rect_bool(int x0, int y0, int x1, int y1)
{
	line_bool(x0, y0, x0, y1);
	line_bool(x0, y1, x1, y1);
	line_bool(x1, y1, x1, y0);
	line_bool(x0, y0, x1, y0);
	
/*    if (!filled) {
	line_bool(x0, y0, x0, y1);
	line_bool(x0, y1, x1, y1);
	line_bool(x1, y1, x1, y0);
	line_bool(x0, y0, x1, y0);
    } else {
	int x, y;
	int _x, _y;
	
	if (y1 < y0) {
	    _y = y1;
	    y1 = y0;
	    y0 = _y;
	}

	if (x1 < x0) {
	    _x = x1;
	    x1 = x0;
	    x0 = _x;
	}

	for (y = y0; y < y1; y++) {
	    for (x = x0;x < x1; x++) {
		clearpixel_bool(x, y);
	    }
	} 
    }*/
}

void rect(int x0, int y0, int x1, int y1, int clear)
{
	line(x0, y0, x0, y1, clear);
	line(x0, y1, x1, y1, clear);
	line(x1, y1, x1, y0, clear);
	line(x0, y0, x1, y0, clear);

    /*if (!filled) {
	line(x0, y0, x0, y1, clear);
	line(x0, y1, x1, y1, clear);
	line(x1, y1, x1, y0, clear);
	line(x0, y0, x1, y0, clear);
    }
    else {
	int x, y;
	int _x, _y;
	
	if (y1 < y0) {
	    _y = y1;
	    y1 = y0;
	    y0 = _y;
	}

	if (x1 < x0) {
	    _x = x1;
	    x1 = x0;
	    x0 = _x;
	}
	
	for (y = y0; y < y1; y++) {
	    for (x = x0;x < x1; x++) {
		putpixel_all(x, y, clear);
	    }
	} 
    }*/
}

void ellipse_bool(int cx, int cy, int xr, int yr)
{
    int x,y;
    int xc, yc;
    int ee;
    int tas, tbs;
    int stx, sty;

    if (!xr || !yr) {
	return;
    }
    
    printf("radius %d %d\n", xr, yr);
        
    tas = 2 * xr * xr;
    tbs = 2 * yr * yr;
    
    x = xr;
    y = 0;
    
    xc = yr * yr * ( 1 - 2 * xr);
    yc = xr * xr;
    ee = 0;
    
    stx = tbs * xr;
    sty = 0;
    
    while (stx >= sty) {
	clearpixel_bool(cx + x, cy + y);
	clearpixel_bool(cx - x, cy + y);
	clearpixel_bool(cx + x, cy - y);
	clearpixel_bool(cx - x, cy - y);
	y++;
	sty += tas;
	ee += yc;
	yc += tas;
	if ((2 * ee + xc) > 0) {
	    x--;
	    stx -= tbs;
	    ee += xc;
	    xc += tbs;
	}
    }
    
    x = 0;
    y = yr;
    xc = yr * yr;
    yc = xr * xr * (1 - 2 * yr);
    ee = 0;
    stx = 0;
    sty = tas * yr;
    
    while (stx <= sty) {
	clearpixel_bool(cx + x, cy + y);
	clearpixel_bool(cx - x, cy + y);
	clearpixel_bool(cx + x, cy - y);
	clearpixel_bool(cx - x, cy - y);
	x++;
	stx += tbs;
	ee += xc;
	xc += tbs;
	
	if ((2 * ee + xc) > 0) {
	    y--;
	    sty -= tas;
	    ee += yc;
	    yc += tas;
	}
    }
}

void ellipse (int cx, int cy, int xr, int yr, int clear)
{
    int x,y;
    int xc, yc;
    int ee;
    int tas, tbs;
    int stx, sty;

    
    if (!xr || !yr) {
	return;
    }
    
    printf("radius %d %d\n", xr, yr);
        
    tas = 2 * xr * xr;
    tbs = 2 * yr * yr;
    
    x = xr;
    y = 0;
    
    xc = yr * yr * ( 1 - 2 * xr);
    yc = xr * xr;
    ee = 0;
    
    stx = tbs * xr;
    sty = 0;
    
    while (stx >= sty) {
	putpixel_all(cx + x, cy + y, clear);
	putpixel_all(cx - x, cy + y, clear);
	putpixel_all(cx + x, cy - y, clear);
	putpixel_all(cx - x, cy - y, clear);
	y++;
	sty += tas;
	ee += yc;
	yc += tas;
	if ((2 * ee + xc) > 0) {
	    x--;
	    stx -= tbs;
	    ee += xc;
	    xc += tbs;
	}
    }
    
    x = 0;
    y = yr;
    xc = yr * yr;
    yc = xr * xr * (1 - 2 * yr);
    ee = 0;
    stx = 0;
    sty = tas * yr;
    
    while (stx <= sty) {
	putpixel_all(cx + x, cy + y, clear);
	putpixel_all(cx - x, cy + y, clear);
	putpixel_all(cx + x, cy - y, clear);
	putpixel_all(cx - x, cy - y, clear);
	x++;
	stx += tbs;
	ee += xc;
	xc += tbs;
	
	if ((2 * ee + xc) > 0) {
	    y--;
	    sty -= tas;
	    ee += yc;
	    yc += tas;
	}
    }
}

void fill(int x, int y, int clear)
{
	
	g_print("FILL\n");
	
	if (is_point_set(x, y, clear)) {
		return;
	} else {
		GQueue *q = g_queue_new();
		Point *p;
		
		p = g_new0(Point, 1);
		p->x = x;
		p->y = y;
		g_queue_push_tail(q, p);
		
		while (!g_queue_is_empty(q)) {
			Point *_p;

			_p = g_queue_pop_tail(q);

			x = _p->x;
			y = _p->y;
			
			g_free(_p);
			
			if (is_point_set(x, y, clear))
				continue;

			putpixel_all(x, y, clear);
			
			_p = g_new0(Point, 1);
			_p->x = x + 1;
			_p->y = y;
			g_queue_push_tail(q, _p);

			_p = g_new0(Point, 1);
			_p->x = x;
			_p->y = y + 1;
			g_queue_push_tail(q, _p);
			
			_p = g_new0(Point, 1);
			_p->x = x - 1;
			_p->y = y;
			g_queue_push_tail(q, _p);
			
			_p = g_new0(Point, 1);
			_p->x = x;
			_p->y = y - 1;
			g_queue_push_tail(q, _p);
		}
	}
}
