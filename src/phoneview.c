/* $Id$ */

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"
#include "utils.h"

static GladeXML *phone_xml = NULL;
extern GMutex *phoneview_mutex;
extern gboolean phoneview_updated;

void gnocky_phone_view_destroy(GtkWidget *phone_view)
{
	if (phone_view)
		gtk_widget_destroy(phone_view);
	if (phone_xml)
		g_object_unref(phone_xml);
}

GtkWidget *gnocky_phone_view_create()
{
	GtkWidget *pv;

	phone_xml = create_gladexml("gnocky-phone-view.glade", "phone_view");

	if (!phone_xml) {
		g_error("Cannot create phone view!\n");
		gnocky_main_quit();
	}

	pv = glade_xml_get_widget(phone_xml, "phone_view");

	if (pv == NULL) {
		g_error("uhm... pv == NULL!!!\n");
	}
	return pv;
}

void gnocky_phone_view_update(GnockyPhoneMonitor pm)
{
	GtkWidget *widget;
	gchar *tmp;
	const gchar *aux;

	if (!g_mutex_trylock(phoneview_mutex)) return;

	/* rf level */
	widget = glade_xml_get_widget(phone_xml, "rf_progress");
	gtk_progress_set_value(GTK_PROGRESS(widget), (gdouble)pm.rf_level);

	if (pm.rf_level == -1)
		tmp = g_strdup_printf(_("N/A"));
	else
		tmp = g_strdup_printf("%d%%", (int)pm.rf_level);

	gtk_progress_bar_set_text(GTK_PROGRESS_BAR(widget), tmp);
	g_free(tmp);

	/* battery level */
	widget = glade_xml_get_widget(phone_xml, "battery_progress");
	gtk_progress_set_value(GTK_PROGRESS(widget), (gdouble)pm.battery_level);

	if (pm.battery_level == -1)
		tmp = g_strdup_printf(_("N/A"));
	else
		tmp = g_strdup_printf("%d%%", (int)pm.battery_level);

	gtk_progress_bar_set_text(GTK_PROGRESS_BAR(widget), tmp);
	g_free(tmp);

	widget = glade_xml_get_widget(phone_xml, "network_info_label");
	if (pm.network_name && pm.network_country) {
		tmp = g_strdup_printf("<b>%s</b> (%s)", pm.network_name, pm.network_country);
		gtk_label_set_markup(GTK_LABEL(widget), tmp);
		g_free(tmp);
	} else {
		gtk_label_set_text(GTK_LABEL(widget), _("N/A"));
	}

	if (phoneview_updated) {
		GdkPixbuf *pixbuf;
		widget = glade_xml_get_widget(phone_xml, "phone_image");
		pixbuf = gtk_image_get_pixbuf(GTK_IMAGE(widget));
		if (!pixbuf) {
			gchar *model = g_ascii_strdown(pm.model, -1);

			tmp = g_strconcat("/phones/", model, ".png", NULL);
			pixbuf = create_pixbuf(tmp);
			g_free(model);
			g_free(tmp);
		
			if (!pixbuf) {
				tmp = g_strconcat("/phones/", gn_model_get(pm.model), ".png", NULL);
				pixbuf = create_pixbuf(tmp);
				g_free(tmp);
			}
		
			if (!pixbuf) {
				pixbuf = create_pixbuf("/phones/unknown.png");
			}

			if (pixbuf) {
				gtk_image_set_from_pixbuf(GTK_IMAGE(widget), pixbuf);
				gdk_pixbuf_unref(pixbuf);
			}
		}

		if (pm.model) {
			aux = gn_model_get(pm.model);
			if (!aux) {
				tmp = g_strdup(pm.model);
			} else {
				tmp = g_strdup_printf("%s (%s)", aux, pm.model);
			}
			widget = glade_xml_get_widget(phone_xml, "model_entry");
			gtk_label_set_text(GTK_LABEL(widget), tmp);
			g_free(tmp);
			//gtk_entry_set_text(GTK_ENTRY(widget), pm.model);
		}

		if (pm.manufacturer) {
			widget = glade_xml_get_widget(phone_xml, "manufacturer_entry");
			gtk_label_set_text(GTK_LABEL(widget), pm.manufacturer);
			//gtk_entry_set_text(GTK_ENTRY(widget), pm.manufacturer);
		}

		if (pm.revision) {
			widget = glade_xml_get_widget(phone_xml, "rev_entry");
			gtk_label_set_text(GTK_LABEL(widget), pm.revision);
			//gtk_entry_set_text(GTK_ENTRY(widget), pm.revision);
		}
	
		if (pm.imei) {
			widget = glade_xml_get_widget(phone_xml, "imei_entry");
			tmp = g_strdup_printf("<b>%s</b>", pm.imei);
			gtk_label_set_markup(GTK_LABEL(widget), tmp);
			//gtk_entry_set_text(GTK_ENTRY(widget), pm.imei);
			g_free(tmp);
		}
		phoneview_updated = FALSE;
	}

	g_mutex_unlock(phoneview_mutex);
}



