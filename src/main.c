/* $Id$ */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"
#include "monitor.h"
#include "utils.h"
#include "statusbar.h"
#include "preferences.h"

#include "phoneview.h"
#include "phonebookview.h"
#include "smsview.h"
#include "logosview.h"

/* GUI stuff */
GladeXML *xml;
GtkWidget *window;
GtkWidget *main_view;
GtkWidget *phone_view;
GtkWidget *phonebook_view;
GtkWidget *sms_view;
GtkWidget *logos_view;
GtkWidget *current_view = NULL;

GtkTooltips *tooltips = NULL;
static guint view_timeout_id = 0;

extern GAsyncQueue *events;
extern GThread *monitor;
extern GnockyPhoneMonitor pm;
extern struct gn_statemachine statemachine;

void gnocky_set_current_view(GtkWidget *view)
{
	if (current_view == view)
		return;
	
	if (current_view) {
		gtk_widget_ref(current_view);
		gtk_container_remove(GTK_CONTAINER(main_view), current_view);
		/* FIXME - needed to keep recount at reasonable level*/
		if (G_OBJECT(current_view)->ref_count >= 2) {
			gtk_widget_unref(current_view);
		}
	}
	gtk_container_add(GTK_CONTAINER(main_view), view);
	current_view = view;
}

gint gnocky_view_update(gpointer data)
{
	gnocky_statusbar_update();
	
	gnocky_phone_view_update(pm);
	gnocky_phonebook_view_update(pm);
	gnocky_sms_view_update(pm);
	
	return TRUE;
}

void gnocky_button_menu_clicked(GtkWidget *widget, gpointer data)
{
	g_print("button clicked\n");
	gnocky_set_current_view(data);
}

void on_preferences_button_clicked(GtkWidget *widget, gpointer data)
{
	create_preferences(&statemachine.config);
}

void on_about_activate(GtkWidget *widget, gpointer data)
{
	GladeXML *about_xml = NULL;
	GtkWidget *av;

	about_xml = create_gladexml("gnocky-about.glade", "aboutdialog");

	if (!about_xml)
		g_error("Cannot create about dialog!\n");

	glade_xml_signal_autoconnect(about_xml);

	av = glade_xml_get_widget(about_xml, "aboutdialog");

	if (av == NULL)
		g_error("Wrong glade data!\n");

	g_signal_connect_swapped (av, "response",
				  G_CALLBACK (gtk_widget_destroy), 
				  av);

	gtk_window_set_transient_for (GTK_WINDOW (av), GTK_WINDOW (window));

	gtk_widget_show_all(av);
}

void gnocky_set_button_active(gboolean active)
{
	GtkWidget *button;

	if (!active) {
		button = glade_xml_get_widget(xml, "phone_button");
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(button), TRUE);
	}

	button = glade_xml_get_widget(xml, "phonebook_button");
	gtk_widget_set_sensitive(button, active);

	button = glade_xml_get_widget(xml, "sms_button");
	gtk_widget_set_sensitive(button, active);

	button = glade_xml_get_widget(xml, "logos_button");
	if (active && g_strcasecmp(statemachine.config.model, "AT"))
		gtk_widget_set_sensitive(button, TRUE);
	else
		gtk_widget_set_sensitive(button, FALSE);
}

void gnocky_set_main_gui_callbacks()
{
	GtkWidget *button;
	
	button = glade_xml_get_widget(xml, "phone_button");
	
	if (button)
		g_signal_connect(G_OBJECT(button), "clicked", (GCallback) gnocky_button_menu_clicked, phone_view);
	
	button = glade_xml_get_widget(xml, "phonebook_button");
	if (button)
		g_signal_connect(G_OBJECT(button), "clicked", (GCallback) gnocky_button_menu_clicked, phonebook_view);
	gtk_widget_set_sensitive(button, FALSE);
	
	button = glade_xml_get_widget(xml, "sms_button");
	if (button)
		g_signal_connect(G_OBJECT(button), "clicked", (GCallback) gnocky_button_menu_clicked, sms_view);
	gtk_widget_set_sensitive(button, FALSE);
	
	button = glade_xml_get_widget(xml, "logos_button");
	if (button)
		g_signal_connect(G_OBJECT(button), "clicked", (GCallback) gnocky_button_menu_clicked, logos_view);
	gtk_widget_set_sensitive(button, FALSE);
}

void gnocky_interface_destroy()
{
	if (view_timeout_id)
		g_source_remove(view_timeout_id);

	gnocky_logos_view_destroy(logos_view);
	gnocky_sms_view_destroy(sms_view);
	gnocky_phonebook_view_destroy(phonebook_view);
	gnocky_phone_view_destroy(phone_view);

	gnocky_statusbar_terminate();

	if (main_view)
		gtk_widget_destroy(main_view);
/* this will be destroyed later by GTK
	if (window)
		gtk_widget_destroy(window);
*/
	if (xml)
		g_object_unref(xml);
}

void gnocky_interface_create()
{
	/* main interface */
	xml = create_gladexml("gnocky.glade", NULL);

	if (!xml) {
		gnocky_show_error("Cannot create user interface!\n");
		gtk_main_quit();
	}

	glade_xml_signal_autoconnect(xml);
	window = glade_xml_get_widget(xml, "window");

	main_view = glade_xml_get_widget(xml, "main_view");
	
	gnocky_statusbar_init();

	/* views creation */
	phone_view = gnocky_phone_view_create();
	phonebook_view = gnocky_phonebook_view_create();
	sms_view = gnocky_sms_view_create();
	logos_view = gnocky_logos_view_create();

	gnocky_set_main_gui_callbacks();
	
	/* setting the default view */
	gnocky_set_current_view(phone_view);

	tooltips = gtk_tooltips_new();
	
	gtk_tooltips_enable(tooltips);
	
	view_timeout_id = g_timeout_add(1000, gnocky_view_update, NULL);
}

void gnocky_main_quit()
{
    gtk_main_quit();
}

int
main (int argc, char *argv[])
{

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	g_thread_init(NULL);
	gtk_init(&argc, &argv);

	events = g_async_queue_new();

	gnocky_interface_create();

	gnocky_monitor_create();

	gtk_main ();

	gnocky_monitor_destroy();

	gnocky_interface_destroy();

	return 0;
}
