/* $Id$ */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glade/glade.h>
#include <gnokii.h>
#include <string.h>

#include "gnocky.h"
#include "utils.h"
#include "phonebookview.h"

extern GtkWidget *window;
extern GSList *sms_list;
extern GMutex *sms_mutex;
extern gboolean sms_updated;
extern GSList *phonebook_list;
extern GMutex *phonebook_mutex;
extern GdkPixbuf *me_pixbuf;
extern GdkPixbuf *sm_pixbuf;

static GladeXML *sms_xml = NULL;
static GladeXML *new_sms_xml = NULL;
static GtkListStore *store;
static GtkTreeIter iter;

enum {
	COL_SENDER,
	COL_DATE,
	COL_MEMORY,
	COL_POINTER
};

enum {
	COL_TYPE,
	COL_NAME,
	COL_PB_POINTER
};

static GtkTreeModel *gnocky_sms_create_model()
{
  /* create list store */
  store = gtk_list_store_new (4, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);

  return GTK_TREE_MODEL (store);
}

static gint gnocky_sms_sort_date_column(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b, gpointer user_data)
{
	gn_sms *smsa, *smsb;
	gint ret;

	gtk_tree_model_get (model, a, COL_POINTER, &smsa, -1);
	gtk_tree_model_get (model, b, COL_POINTER, &smsb, -1);

	/* this ignores smsc_time.timezone */
	ret = smsa->smsc_time.year - smsb->smsc_time.year;
	if (ret) return ret;
	ret = smsa->smsc_time.month - smsb->smsc_time.month;
	if (ret) return ret;
	ret = smsa->smsc_time.day - smsb->smsc_time.day;
	if (ret) return ret;
	ret = smsa->smsc_time.hour - smsb->smsc_time.hour;
	if (ret) return ret;
	ret = smsa->smsc_time.minute - smsb->smsc_time.minute;
	if (ret) return ret;
	ret = smsa->smsc_time.second - smsb->smsc_time.second;

	return ret;
}

static void gnocky_sms_add_columns(GtkTreeView *treeview)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model (treeview);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Sender"),
						     renderer,
						     "text",
						     COL_SENDER,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_SENDER);
	gtk_tree_view_append_column (treeview, column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Date"),
						     renderer,
						     "text",
						     COL_DATE,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_DATE);
	gtk_tree_view_append_column (treeview, column);
	
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Memory"),
						     renderer,
						     "text",
						     COL_MEMORY,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_MEMORY);
	gtk_tree_view_append_column (treeview, column);
	
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE(model), COL_DATE, gnocky_sms_sort_date_column, NULL, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(model), COL_DATE, GTK_SORT_DESCENDING);
}

gboolean gnocky_new_sms_count_characters(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
	GtkTextBuffer *buffer;
	GtkWidget *w;
	gint char_count;
	gchar *tmp;
	
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(widget));
	char_count = gtk_text_buffer_get_char_count (buffer);
	
	if (char_count > GN_SMS_MAX_LENGTH) {
		GtkTextIter start, end;
		gchar *text;
		
		gtk_text_buffer_get_start_iter(buffer, &start);
		gtk_text_buffer_get_end_iter(buffer, &end);
		
		text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
		gtk_text_buffer_set_text(buffer, text, GN_SMS_MAX_LENGTH);
		g_free(text);
		char_count = GN_SMS_MAX_LENGTH;
	}
	w = glade_xml_get_widget(new_sms_xml, "chars_label");
	tmp = g_strdup_printf(_("%d left"), GN_SMS_MAX_LENGTH - char_count);
	gtk_label_set_markup(GTK_LABEL(w), tmp);
	g_free(tmp);
	
	return FALSE;
}

static GtkTreeModel *gnocky_sms_phone_search_model()
{
	GtkListStore *store;
  	/* create list store */
  	store = gtk_list_store_new (3, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_POINTER);

	return GTK_TREE_MODEL (store);
}

static void gnocky_phone_search_add_columns(GtkTreeView *treeview)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model (treeview);
	
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Type"),
						     renderer,
						     "pixbuf",
						     COL_TYPE,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_TYPE);
	gtk_tree_view_append_column (treeview, column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
						     renderer,
						     "text",
						     COL_NAME,
						     NULL);
	gtk_tree_view_column_set_sort_column_id (column, COL_NAME);
	gtk_tree_view_append_column (treeview, column);

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(model), COL_NAME, GTK_SORT_ASCENDING);
}

gboolean pbs_clicked(GtkWidget *widget, GdkEventButton *ev, gpointer user_data)
{
	if (ev->type == GDK_2BUTTON_PRESS && ev->button == 1) {
		GtkWidget *dialog;
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
		GtkTreeViewColumn *treevc = NULL;
		GtkTreePath *treepath = NULL;
		GtkTreeIter iter;
		gn_phonebook_entry *en;

		if (!gtk_tree_view_get_path_at_pos
		    (GTK_TREE_VIEW(widget), ev->x, ev->y, &treepath, &treevc, NULL, NULL))
			return FALSE;
		gtk_tree_model_get_iter(model, &iter, treepath);
		gtk_tree_path_free(treepath);
		gtk_tree_model_get(model, &iter, COL_PB_POINTER, &en, -1);
		if (en) {
		    dialog = gtk_widget_get_ancestor(widget, GTK_TYPE_DIALOG);
		    gtk_dialog_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
		}
	}
	return FALSE;
}

void gnocky_sms_phonebook_search_clicked(GtkWidget *widget, gpointer user_data)
{
	GtkWidget *phone_search;
	GtkWidget *phone_tree;
	GtkTreeModel *model;
	int res;
 	GladeXML *phone_search_xml = create_gladexml("gnocky-phonebook-list-dialog.glade", "phonebook_list_dialog");

	if (!phone_search_xml)
		g_error("Cannot create phone search view!\n");
	
	phone_search = glade_xml_get_widget(phone_search_xml, "phonebook_list_dialog");
	glade_xml_signal_autoconnect(phone_search_xml);

	model = gnocky_sms_phone_search_model();
	phone_tree = glade_xml_get_widget(phone_search_xml, "phonebook_treeview");
	gtk_tree_view_set_model(GTK_TREE_VIEW(phone_tree), model);
	
	gnocky_phone_search_add_columns(GTK_TREE_VIEW(phone_tree));

	if (g_mutex_trylock(phonebook_mutex)) {
		GSList *tmp = phonebook_list;
		while (tmp) {
			gn_phonebook_entry *en = (gn_phonebook_entry *) tmp->data;
			gchar *number_string = g_strdup("");
			gchar *name = g_locale_to_utf8(en->name, -1, NULL, NULL, NULL);

			if (en->subentries_count) {
				gint i;
				gchar *tmp;
					
				for (i = 0;i<en->subentries_count;i++) {
					tmp = g_strdup_printf("%s %s", number_string, en->subentries[i].data.number);
					g_free(number_string);
					number_string = tmp;
				}
			} else {
				g_free(number_string);
				number_string = g_strdup(en->number);
			}
				
			gtk_list_store_append (GTK_LIST_STORE(model), &iter);
			gtk_list_store_set (GTK_LIST_STORE(model), &iter, COL_NAME, name, COL_PB_POINTER, en, -1);
				
			g_free(name);
			g_free(number_string);
				
			switch (en->memory_type) {
				case GN_MT_ME:
					gtk_list_store_set (GTK_LIST_STORE(model), &iter, COL_TYPE, me_pixbuf, -1);
					break;
				case GN_MT_SM:
					gtk_list_store_set (GTK_LIST_STORE(model), &iter, COL_TYPE, sm_pixbuf, -1);
					break;
				default:
					break;
			}
			tmp = tmp->next;
		}
	}
	g_mutex_unlock(phonebook_mutex);

	gtk_window_set_transient_for (GTK_WINDOW (phone_search), GTK_WINDOW (((GnockyNewSmsInfo *) user_data)->parent));
	res = gtk_dialog_run(GTK_DIALOG(phone_search));
	if (res == GTK_RESPONSE_OK) {
		GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(phone_tree));
		GtkTreeIter iter;
		GList *rows, *tmp;
		gn_phonebook_entry *en;

		rows = gtk_tree_selection_get_selected_rows(selection, NULL);
		tmp = rows;
		while (tmp) {
			gtk_tree_model_get_iter(model, &iter, tmp->data);
			gtk_tree_model_get(model, &iter, COL_PB_POINTER, &en, -1);
			/* this needs to be fixed when multipile selection
			    will be allowed */
			if (en)
			    gtk_entry_set_text(GTK_ENTRY(((GnockyNewSmsInfo *) user_data)->sms_number), en->number);
			tmp = tmp->next;
		}
		g_list_foreach (rows, (GFunc) gtk_tree_path_free, NULL);
		g_list_free (rows);
		
/*		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
		GtkTreeViewColumn *treevc = NULL;
		GtkTreePath *treepath = NULL;
		GtkTreeIter iter;
		gn_phonebook_entry *en;

		if (!gtk_tree_view_get_path_at_pos
		    (GTK_TREE_VIEW(widget), ev->x, ev->y, &treepath, &treevc, NULL, NULL))
			return FALSE;
		gtk_tree_model_get_iter(model, &iter, treepath);
		gtk_tree_model_get(model, &iter, COL_PB_POINTER, &en, -1);
*/
	}
	gtk_widget_destroy(phone_search);
	g_object_unref(phone_search_xml);
	g_object_unref (model);

}

void gnocky_new_sms_clicked(GtkWidget *widget, gpointer data)
{
	GtkWidget *new_sms;
	GtkWidget *phonebook_search_button;
	GnockyNewSmsInfo new_sms_info;
	int res;
	
	new_sms_xml = create_gladexml("gnocky-new-sms.glade", "new_sms_dialog");
	
	if (!new_sms_xml)
		g_error("Cannot create new sms view!\n");
	
	new_sms = glade_xml_get_widget(new_sms_xml, "new_sms_dialog");
	glade_xml_signal_autoconnect(new_sms_xml);
	
	/* set signal by hand... */
	phonebook_search_button = glade_xml_get_widget(new_sms_xml, "phonebook_search_button");
	new_sms_info.sms_number = glade_xml_get_widget(new_sms_xml, "sms_number");
	new_sms_info.parent = GTK_WINDOW(new_sms);
	g_signal_connect(G_OBJECT(phonebook_search_button), "clicked", 
			 G_CALLBACK(gnocky_sms_phonebook_search_clicked), &new_sms_info);

	gtk_window_set_transient_for (GTK_WINDOW (new_sms), GTK_WINDOW (window));
	res = gtk_dialog_run(GTK_DIALOG(new_sms));
	
	gnocky_new_sms_count_characters(glade_xml_get_widget(new_sms_xml, "sms_text"), NULL, NULL);
	
	if (res == GTK_RESPONSE_OK) {
		GtkWidget *entry;
		gchar *number, *text, *sms_text;
		GtkTextBuffer *buffer;
		GtkTextIter start, end;
		gn_sms *sms;
		
		entry = glade_xml_get_widget(new_sms_xml, "sms_number");
		number = g_locale_from_utf8(gtk_entry_get_text(GTK_ENTRY(entry)), -1, NULL, NULL, NULL);
		
		entry = glade_xml_get_widget(new_sms_xml, "sms_text");
		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(entry));
		
		gtk_text_buffer_get_start_iter(buffer, &start);
		gtk_text_buffer_get_iter_at_offset(buffer, &end, GN_SMS_MAX_LENGTH);
		
		text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
		sms_text = g_locale_from_utf8(text, -1, NULL, NULL, NULL);
		g_free(text);
		
		sms = g_new0(gn_sms, 1);
		gn_sms_default_submit(sms);
		strncpy(sms->remote.number, number, sizeof(sms->remote.number) - 1);
		if (number[0] == '+') {
			sms->remote.type = GN_GSM_NUMBER_International;
		} else {
			sms->remote.type = GN_GSM_NUMBER_Unknown;
		}
		strncpy(sms->user_data[0].u.text, sms_text, strlen(sms_text));
		sms->user_data[0].length = strlen(sms_text);
		
		sms->user_data[0].type = GN_SMS_DATA_Text;
		if (!gn_char_def_alphabet(sms->user_data[0].u.text)) 
			sms->dcs.u.general.alphabet = GN_SMS_DCS_UCS2;
		sms->user_data[1].type = GN_SMS_DATA_None;
		g_free(number);
		g_free(sms_text);
		
		gnocky_event_push(GNOCKY_EVENT_SEND_SMS, sms);
	}
	
	gtk_widget_destroy(new_sms);
	g_object_unref(new_sms_xml);
	new_sms_xml = NULL;
}

void gnocky_sms_load_from_phone_clicked(GtkWidget *widget, gpointer data)
{
	gnocky_event_push(GNOCKY_EVENT_READ_SMS_LIST, NULL);
}

gboolean sms_pressed(GtkWidget *widget, GdkEventKey *ev, gpointer user_data)
{
	if (ev->keyval == GDK_Delete) {
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
		GtkTreePath *treepath = NULL;
		GtkTreeIter iter;
		GtkWidget *dialog;
		gint res;
		gn_sms *sms;
		
		gtk_tree_view_get_cursor (GTK_TREE_VIEW(widget), &treepath, NULL);
		if (!treepath) return FALSE;
		gtk_tree_model_get_iter(model, &iter, treepath);
		gtk_tree_model_get(model, &iter, COL_POINTER, &sms, -1);
		
		dialog = gtk_message_dialog_new(GTK_WINDOW(window), GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION, 
                                             GTK_BUTTONS_OK_CANCEL,
					     _("Do you want to delete this SMS?"));

		res = gtk_dialog_run(GTK_DIALOG(dialog));
		if (res == GTK_RESPONSE_OK) {
			gnocky_event_push(GNOCKY_EVENT_DELETE_SMS, sms);
		}
		gtk_tree_path_free(treepath);

		gtk_widget_destroy(dialog);
	}
	return FALSE;
}

static gboolean sms_clicked(GtkWidget *treeview, GdkEventButton *ev, gpointer user_data)
{
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	GtkTreePath *treepath = NULL;
	GtkTreeIter iter;
	GtkWidget *widget;
	GtkTextBuffer *buffer;
	gn_sms *sms;
	gchar *tmp, *sender;
	gchar *utf_body;
		
	gtk_tree_view_get_cursor (GTK_TREE_VIEW(treeview), &treepath, NULL);
	if (!treepath) return FALSE;
	gtk_tree_model_get_iter(model, &iter, treepath);
	gtk_tree_path_free(treepath);
	gtk_tree_model_get(model, &iter, COL_POINTER, &sms, -1);
	
	widget = glade_xml_get_widget(sms_xml, "sms_body");
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
	utf_body = g_locale_to_utf8(sms->user_data[0].u.text, -1, NULL, NULL, NULL);
	gtk_text_buffer_set_text (buffer, utf_body, -1);
	g_free(utf_body);
	
	widget = glade_xml_get_widget(sms_xml, "from_label");

	sender = gnocky_pb_get_name(sms->remote.number);

	if (!sender) {
		tmp = g_strdup(sms->remote.number);
	} else {
		gchar *utf_sender = g_locale_to_utf8(sender, -1, NULL, NULL, NULL);
		tmp = g_strdup_printf("<b>%s</b> (%s)", utf_sender, sms->remote.number);
		g_free(sender);
		g_free(utf_sender);
	}
	
	gtk_label_set_markup(GTK_LABEL(widget), tmp);
	g_free(tmp);
	
	widget = glade_xml_get_widget(sms_xml, "date_label");
	tmp = g_strdup_printf("<b>%02d/%02d/%04d %02d:%02d</b>", 
				sms->smsc_time.day, sms->smsc_time.month, sms->smsc_time.year, sms->smsc_time.hour, sms->smsc_time.minute);
	gtk_label_set_markup(GTK_LABEL(widget), tmp);
	g_free(tmp);

	return FALSE;
}

void gnocky_sms_view_destroy(GtkWidget *sms_view)
{
	if (sms_view)
		gtk_widget_destroy(sms_view);
	if (sms_xml)
		g_object_unref(sms_xml);
}

GtkWidget *gnocky_sms_view_create()
{
	GtkTreeModel *model;
	GtkWidget *sms_tree;
	GtkWidget *sv;
	GtkTreeSelection *selection;

	sms_xml = create_gladexml("gnocky-sms-view.glade", "sms_view");

	if (!sms_xml) {
		g_error("Cannot create sms view!\n");
		gnocky_main_quit();
	}
	
	glade_xml_signal_autoconnect(sms_xml);
	
	sv = glade_xml_get_widget(sms_xml, "sms_view");

	if (sv == NULL) {
		g_error("uhm... sv == NULL!!!\n");
	}
	
	model = gnocky_sms_create_model();
	sms_tree = glade_xml_get_widget(sms_xml, "sms_tree");
	gtk_tree_view_set_model(GTK_TREE_VIEW(sms_tree), model);
	g_object_unref (model);
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(sms_tree));
	g_signal_connect_swapped (G_OBJECT (selection), "changed", G_CALLBACK (sms_clicked), G_OBJECT (sms_tree));
	
	gnocky_sms_add_columns(GTK_TREE_VIEW(sms_tree));
	
	return sv;
}

void gnocky_sms_view_update(GnockyPhoneMonitor pm)
{
	if (g_mutex_trylock(sms_mutex)) {
		if (sms_updated) {
			GSList *tmp = sms_list;
			gtk_widget_set_sensitive(glade_xml_get_widget(sms_xml, "sms_save_to_phone"), tmp != NULL);
			gtk_list_store_clear(store);
			while (tmp) {
				gn_sms *sms = tmp->data;
				gchar *sender;
				gchar *date;
				
				sender = gnocky_pb_get_name(sms->remote.number);
				if (!sender) {
					sender = g_strdup(sms->remote.number);
				} else {
					gchar *utf_sender = g_locale_to_utf8(sender, -1, NULL, NULL, NULL);
					g_free(sender);
					sender = utf_sender;
				}
				date = g_strdup_printf("%02d/%02d/%04d %02d:%02d",
				sms->smsc_time.day, sms->smsc_time.month, sms->smsc_time.year, sms->smsc_time.hour, sms->smsc_time.minute);
				
				gtk_list_store_append (store, &iter);
				gtk_list_store_set (store, &iter, COL_SENDER, sender, COL_DATE, date, COL_MEMORY, gn_memory_type2str(sms->memory_type), COL_POINTER, sms, -1);
				
				g_free(sender);
				g_free(date);
				tmp = tmp->next;
			}
			sms_updated = FALSE;
		}
		g_mutex_unlock(sms_mutex);
	}
}
