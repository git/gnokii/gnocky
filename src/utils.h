/* $Id$ */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <glib.h>
#include <glade/glade.h>
#include "gnocky.h"

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif


/* standard message dialogs wrappers */
void gnocky_show_warning(gchar *str);
void gnocky_show_error(gchar *str);
void gnocky_show_message(gint type, gchar *str);

/* messages from threads */
void gnocky_thread_messages_init();
void gnocky_show_error_from_thread(gchar *str);
void gnocky_show_warning_from_thread(gchar *str);
void gnocky_show_message_from_thread(gint type, gchar *str);

/* events */
void gnocky_event_push(gint type, gpointer data);

/* pixbuf creation */
GdkPixbuf *create_pixbuf(const gchar * filename);

/* GladeXML */
GladeXML *create_gladexml(const gchar * filename, const gchar *root);

#endif
