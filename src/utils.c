/* $Id$ */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <unistd.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "gnocky.h"
#include "utils.h"

static GAsyncQueue *msg_queue = NULL;
static GIOChannel *msg_channel = NULL;
static gint msg_watch = 0;
static gint msg_write_pipe = 0;

extern GAsyncQueue *events;
extern GtkWidget *window;

void gnocky_event_push(gint type, gpointer data)
{
	GnockyEvent *ev = g_new0(GnockyEvent, 1);
	ev->type = type;
	ev->data = data;

	g_async_queue_push(events, ev);
}

static gboolean gnocky_chan_message_from_thread(GIOChannel *source, GIOCondition cond, gpointer data)
{
    GnockyMessage *msg = NULL;

    if ((cond & G_IO_ERR) || (cond & G_IO_HUP) || !msg_queue) {
	
	g_error("IO_ERR || IO_HUP!\n");
	msg_watch = 0;
	g_io_channel_unref(msg_channel);
	
	return FALSE;
    } else if (cond & G_IO_IN) {
        if ((msg = g_async_queue_try_pop(msg_queue)) != NULL) {
	    char c;
	    gint bytes_read;
	    g_io_channel_read_chars(source, &c, sizeof(c), &bytes_read, NULL);
	    gnocky_show_message(msg->type, msg->str);
	    
	    g_free(msg->str);
	    g_free(msg);
	}
    }

    return TRUE;
}

void gnocky_thread_messages_init()
{
    gint p[2];
    
    if (pipe(p) == -1) {
	g_error("cannot create pipe\n");
	return;
    }
    
    msg_channel = g_io_channel_unix_new(p[0]);
    
    if (!msg_channel)	{
	g_error("cannot create channel\n");
	return;
    }
    
    if (!msg_queue)
        msg_queue = g_async_queue_new();
    msg_write_pipe = p[1];
    msg_watch = g_io_add_watch(msg_channel, G_IO_IN|G_IO_ERR|G_IO_HUP, gnocky_chan_message_from_thread, NULL);
}

void gnocky_show_warning(gchar *str) 
{
	gnocky_show_message(GTK_MESSAGE_WARNING, str);
}

void gnocky_show_error(gchar *str) 
{
	gnocky_show_message(GTK_MESSAGE_ERROR, str);
}

void gnocky_show_message(gint type, gchar *str)
{
	GtkWidget *dialog = NULL;

	dialog =
		gtk_message_dialog_new (GTK_WINDOW (window), GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT, type,
				GTK_BUTTONS_OK, "%s", str);

	gtk_widget_show_all (dialog);
	g_signal_connect_swapped (GTK_OBJECT (dialog), "response", G_CALLBACK (gtk_widget_destroy), GTK_OBJECT (dialog));
}

void gnocky_show_warning_from_thread(gchar *str) 
{
	gnocky_show_message_from_thread(GTK_MESSAGE_WARNING, str);
}

void gnocky_show_error_from_thread(gchar *str) 
{
	gnocky_show_message_from_thread(GTK_MESSAGE_ERROR, str);
}

void gnocky_show_message_from_thread(gint type, gchar *str)
{
	GnockyMessage *msg = g_new0(GnockyMessage, 1);
	/* initialize dummy variable to avoid valgrind error "Syscall param write(buf) points to uninitialised byte(s)" */
	char dummy = '\0';
	
	if (!msg_queue) {
		gnocky_thread_messages_init();
	}
	msg->type = type;
	msg->str = g_strdup(str);
	
	g_async_queue_push(msg_queue, msg);

	// notify "the other side" that there is a message
	write(msg_write_pipe, &dummy, sizeof(dummy));
}

/* This is an internally used function to check if a pixmap file exists. */
gchar *check_file_exists(const gchar * directory, const gchar * filename)
{
	gchar *full_filename = NULL;
	
	if (directory != NULL) {
	    full_filename = g_build_filename(directory, filename,NULL);
	} else {
	    full_filename = g_strdup(filename);
	}
	
	if (g_file_test(full_filename,G_FILE_TEST_IS_REGULAR))
		return full_filename;
		
	g_free(full_filename);
	
	return NULL;
}

GdkPixbuf *create_pixbuf(const gchar * filename) {
	gchar 		*found_filename = NULL;
	GdkPixbuf 	*pixbuf		= NULL;
	GSList		*head, *dir	= NULL;

	if (!filename || !filename[0])
		return NULL;

	/* We first try any pixmaps directories set by the application. */
	dir = g_slist_prepend(dir, PACKAGE_DATA_DIR "/pixmaps");
	dir = g_slist_prepend(dir, PACKAGE_SOURCE_DIR "/pixmaps");
	head = dir;

	while (dir) {
		found_filename = check_file_exists((gchar *) dir->data, filename);
		
		if (found_filename)
			break;
			
		dir = dir->next;
	}
	g_slist_free(head);

	/* If we haven't found the pixmap, try the source directory. */
	if (!found_filename) 
		found_filename = check_file_exists("../pixmaps", filename);

	if (!found_filename) {
		g_warning("Couldn't find pixmap file: %s", filename);
		return NULL;
	}

	pixbuf = gdk_pixbuf_new_from_file(found_filename, NULL);
	
	g_free(found_filename);

	return pixbuf;
}

GladeXML *create_gladexml(const gchar * filename, const gchar *root) 
{
	gchar 		*found_filename = NULL;
	GSList		*head, *dir	= NULL;
	GladeXML *xml = NULL;
	
	if (!filename || !filename[0])
		return NULL;

	dir = g_slist_prepend(dir, PACKAGE_DATA_DIR "/glade");
	dir = g_slist_prepend(dir, PACKAGE_SOURCE_DIR "/src/glade");
	head = dir;

	while (dir) {
		found_filename = check_file_exists((gchar *) dir->data, filename);
		
		if (found_filename)
			break;
			
		dir = dir->next;
	}
	g_slist_free(head);

	if (!found_filename) 
		found_filename = check_file_exists("./src/glade", filename);

	if (!found_filename) {
		g_warning("Couldn't find XML file: %s", filename);
		return NULL;
	}
	
	xml = glade_xml_new(found_filename, root, NULL);

	g_free(found_filename);

	return xml;
}
