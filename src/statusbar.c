/* $Id$ */

#include <gtk/gtk.h>
#include <glade/glade.h>
#include "gnocky.h"

extern GladeXML *xml;
extern GnockyPhoneMonitor pm;

/* maybe some day it will be a standalone widget */

static GMutex *statusbar_mutex;
static GnockyStatusBar statusbar = {NULL, NULL, NULL, NULL, 0, 0, 0};

void gnocky_statusbar_set_text(gchar *text)
{
	g_mutex_lock(statusbar_mutex);
	statusbar.text = text;
	g_mutex_unlock(statusbar_mutex);
}

void gnocky_statusbar_progress_set_value(gdouble value)
{
	g_mutex_lock(statusbar_mutex);
	statusbar.progress_value = value;
	g_mutex_unlock(statusbar_mutex);
}

void gnocky_statusbar_progress_show()
{
	g_mutex_lock(statusbar_mutex);
	statusbar.progress_show = TRUE;
	g_mutex_unlock(statusbar_mutex);
}

void gnocky_statusbar_progress_hide()
{
	g_mutex_lock(statusbar_mutex);
	statusbar.progress_show = FALSE;
	g_mutex_unlock(statusbar_mutex);
}

void gnocky_statusbar_update()
{
	g_mutex_lock(statusbar_mutex);
	
	if (statusbar.progress_show) {
		gtk_widget_show(statusbar.progress);
		gtk_progress_set_value(GTK_PROGRESS(statusbar.progress), statusbar.progress_value);
	} else if (GTK_WIDGET_VISIBLE(statusbar.progress)) {
		gtk_widget_hide(statusbar.progress);
	}
	
	if (statusbar.text)
		gtk_statusbar_push(GTK_STATUSBAR(statusbar.statusbar), 0, statusbar.text);
	else
		gtk_statusbar_push(GTK_STATUSBAR(statusbar.statusbar), 0, "");
	
	g_mutex_unlock(statusbar_mutex);
}

gint gnocky_connecting_update(gpointer data)
{
	static gboolean _switch = TRUE; 
	GtkWidget *statusbar_image;
	
	statusbar_image = glade_xml_get_widget(xml, "statusbar_image");
	if (pm.link_status != GNOCKY_LINK_CONNECTED) {
		if (_switch) {
			gtk_image_set_from_pixbuf(GTK_IMAGE(statusbar_image), NULL);
			_switch = FALSE;
		} else {
			gtk_image_set_from_stock(GTK_IMAGE(statusbar_image), GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU);
			_switch = TRUE;
		}
	} else {
		gtk_image_set_from_stock(GTK_IMAGE(statusbar_image), GTK_STOCK_EXECUTE, GTK_ICON_SIZE_MENU);
		return FALSE;
	}
	
	return TRUE;
}

void gnocky_statusbar_conn_indicator()
{
	g_mutex_lock(statusbar_mutex);
	
	statusbar.timeout = g_timeout_add(500, gnocky_connecting_update, NULL);
	g_mutex_unlock(statusbar_mutex);
}

void gnocky_statusbar_terminate()
{
	if (statusbar.timeout)
		g_source_remove(statusbar.timeout);
	if (statusbar.statusbar)
		gtk_widget_destroy(statusbar.statusbar);
	if (statusbar.progress)
		gtk_widget_destroy(statusbar.progress);
	if (statusbar_mutex)
		g_mutex_free(statusbar_mutex);
}

void gnocky_statusbar_init()
{
	statusbar.progress = glade_xml_get_widget(xml, "statusbar_progress");
	statusbar.statusbar = glade_xml_get_widget(xml, "statusbar");
	statusbar_mutex = g_mutex_new();
	statusbar.timeout = 0;
}
