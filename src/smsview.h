/* $Id$ */

#include <gtk/gtk.h>
#include "gnocky.h"

GtkWidget *gnocky_sms_view_create();
void gnocky_sms_view_destroy(GtkWidget *sms_view);
void gnocky_sms_view_update(GnockyPhoneMonitor pm);
