/* $Id$ */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>
#include "utils.h"

extern GtkWidget *window;

enum {
	COL_NAME,
	COL_TYPE
};

void create_preferences(gn_config *cfg)
{
	GladeXML *prefs_xml;
	GtkWidget *prefs;
	GtkWidget *widget;
	GtkTreeStore *store;
	GtkTreeIter iter;
	const gchar *connection_name;
	gn_connection_type connection_type;
	gint i, res;
	
	prefs_xml = create_gladexml("gnocky-preferences.glade", "gnocky_preferences");

	if (!prefs_xml)
		g_error("Cannot create preferences!\n");

	prefs = glade_xml_get_widget(prefs_xml, "gnocky_preferences");

	if (prefs == NULL)
		g_error("Wrong glade data!\n");
	
	widget = glade_xml_get_widget(prefs_xml, "connection");
	store = gtk_tree_store_new(2, G_TYPE_STRING, G_TYPE_INT);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(store), COL_NAME, GTK_SORT_ASCENDING);
	gtk_combo_box_set_model(GTK_COMBO_BOX(widget), GTK_TREE_MODEL(store));
	g_object_unref(store);

	for (i = 0; connection_name = gn_lib_get_supported_connection(i); i++) {
		connection_type = gn_get_connectiontype(connection_name);

		gtk_tree_store_append (store, &iter, NULL);
		gtk_tree_store_set (store, &iter, COL_NAME, connection_name, COL_TYPE, connection_type, -1);

		/* do not use the variable "i" because the combo is sorted
		  (also "i" doesn't correspond to GN_CT_* if some connection types aren't compiled in libgnokii
		  or if positions in libgnokii private array are changed) */
		if (connection_type == cfg->connection_type)
			gtk_combo_box_set_active_iter (GTK_COMBO_BOX(widget), &iter);
	}

	widget = glade_xml_get_widget(prefs_xml, "model_entry");
	gtk_entry_set_text(GTK_ENTRY(widget), cfg->model);
	
	widget = glade_xml_get_widget(prefs_xml, "port_entry");
	gtk_entry_set_text(GTK_ENTRY(widget), cfg->port_device);
	
	/* disable OK and Apply because at this moment changes are ignored in any case */
	widget = glade_xml_get_widget(prefs_xml, "okbutton1");
	gtk_widget_set_sensitive(widget, FALSE);
	widget = glade_xml_get_widget(prefs_xml, "applybutton1");
	gtk_widget_set_sensitive(widget, FALSE);

	gtk_window_set_transient_for(GTK_WINDOW (prefs), GTK_WINDOW (window));
	res = gtk_dialog_run(GTK_DIALOG(prefs));
	
	gtk_widget_destroy(prefs);
	g_object_unref(G_OBJECT(prefs_xml));

	return; 
}
