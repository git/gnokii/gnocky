/* $Id$ */

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gnokii.h>

#include "gnocky.h"

GtkWidget *gnocky_phone_view_create();
void gnocky_phone_view_destroy(GtkWidget *phone_view);
void gnocky_phone_view_update(GnockyPhoneMonitor pm);
