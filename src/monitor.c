/* $Id$ */

/*
 *	Everything here runs in thread. So beware ;-)
 */

#include <unistd.h>
#include <string.h>
#include <glib.h>
#include <gnokii.h>

#include "gnocky.h"
#include "utils.h"
#include "statusbar.h"
#include "logosview.h"

/* internal stuff */
GThread *monitor = NULL;
GnockyPhoneMonitor pm;
GAsyncQueue *events;

GMutex *phoneview_mutex = NULL;
GMutex *phonebook_mutex = NULL;
GMutex *sms_mutex = NULL;
GSList *phonebook_list = NULL;
GSList *sms_list = NULL;
gboolean phoneview_updated = FALSE;
gboolean phonebook_updated = TRUE;
gboolean sms_updated = TRUE;

/* libgnokii stuff */
struct gn_statemachine *statemachine;
static char *bindir;

static gn_error gnocky_phonebook_memory_status()
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;
	gn_memory_status memory_status;
	gn_data_clear(&gdat);

	/* SIM memory stats */
	memory_status.memory_type = GN_MT_SM;
	gdat.memory_status = &memory_status;
	
	if ((error = gn_sm_functions(GN_OP_GetMemoryStatus, &gdat, statemachine)) != GN_ERR_NONE) {
		/* memory not supported */
		pm.ms_sm_free = pm.ms_sm_used = -1;
	} else {
		pm.ms_sm_free = memory_status.free;
		pm.ms_sm_used = memory_status.used;
	}
	
	/* ME memory stats */
	memory_status.memory_type = GN_MT_ME;
	
	if ((error = gn_sm_functions(GN_OP_GetMemoryStatus, &gdat, statemachine)) != GN_ERR_NONE) {
		/* memory not supported */
		pm.ms_me_free = pm.ms_me_used = -1;
	} else {
		pm.ms_me_free = memory_status.free;
		pm.ms_me_used = memory_status.used;
	}
	
	return GN_ERR_NONE;
}

static
void gnocky_monitor_init()
{
	memset(&pm, 0, sizeof(pm));
	pm.power_source = GN_PS_BATTERY;
	pm.manufacturer = g_strdup(_("Unknown"));
	pm.model = g_strdup(_("Unknown"));
	pm.revision = g_strdup(_("Unknown"));
	pm.imei = g_strdup(_("Unknown"));
	pm.rf_level = -1;
	pm.battery_level = -1;
	pm.ms_me_free = pm.ms_me_used = -1;
	pm.ms_sm_free = pm.ms_sm_used = -1;
	pm.network_name = NULL;
	pm.network_country = NULL;

	phoneview_mutex = g_mutex_new();
	phonebook_mutex = g_mutex_new();
	sms_mutex = g_mutex_new();

	/* bitmap init */
	pm.info.operator_logo_width = 78;
	pm.info.operator_logo_height = 21;
	pm.info.caller_logo_width = 72;
	pm.info.caller_logo_height = 28;
	pm.info.startup_logo_width = 96;
	pm.info.startup_logo_height = 60;
	pm.info.models = "";
	pm.info.operator_logo_width = 78;
	pm.info.operator_logo_height = 21;

	gn_bmp_null(&pm.bitmap, &pm.info);
	gn_bmp_resize(&pm.bitmap, GN_BMP_OperatorLogo, &pm.info);
}

static gint gnocky_find_empty_location(gn_memory_type memory_type, gint *location)
{
	gn_error error = GN_ERR_NONE;
	gn_phonebook_entry entry;
	gn_data gdat;
	gint i = 1, max = 0;

	gn_data_clear(&gdat);
	entry.memory_type = memory_type;
	gdat.phonebook_entry = &entry;
	
	/* get memory status only if the user hasn't loaded the phonebook yet
	   because we just need to know "max" which is fixed for each phone or SIM.
	   optimizations like not trying to add if free == 0 must be done by the caller */
	if ((pm.ms_sm_free == -1) && (pm.ms_me_free == -1)) {
		gnocky_phonebook_memory_status();
	}
	switch (entry.memory_type) {
	case GN_MT_ME:
		if (pm.ms_me_free == -1) return GN_ERR_INVALIDMEMORYTYPE;
		max = pm.ms_me_used + pm.ms_me_free;
		break;
	case GN_MT_SM:
		if (pm.ms_sm_free == -1) return GN_ERR_INVALIDMEMORYTYPE;
		max = pm.ms_sm_used + pm.ms_sm_free;
		break;
	default:
		return GN_ERR_INVALIDMEMORYTYPE;
	}

	while (i <= max) {
		entry.location = i;
		entry.empty = 0;
		error = gn_sm_functions(GN_OP_ReadPhonebook, &gdat, statemachine);
		if (error == GN_ERR_INVALIDMEMORYTYPE) {
			return error;
		} else if (error == GN_ERR_NONE) {
			if (entry.empty) {
				*location = i;
				return GN_ERR_NONE;
			} else {
				i++;
			}
		} else if (error == GN_ERR_EMPTYLOCATION) {
			*location = i;
			return GN_ERR_NONE;
		} else  {
			gint err_count = 0;

			while (error != GN_ERR_NONE) {
				if (err_count++ > 3) {
					return error;
				}
				error = gn_sm_functions(GN_OP_ReadPhonebook, &gdat, statemachine);
				sleep(1);
			}
		}
	}
	return GN_ERR_MEMORYFULL;
}

static gn_error gnocky_read_phonebook_mem(gint count, gint max, gn_memory_type memory_type) 
{
	gn_error error = GN_ERR_NONE;
	gn_phonebook_entry entry;
	gn_data gdat;
	gint found = 0;
	gint i = 1;
	
	g_mutex_lock(phonebook_mutex);
	gnocky_statusbar_progress_set_value(0.0);
	gn_data_clear(&gdat);
	gdat.phonebook_entry = &entry;

	while (found < count && i <= max) {
		gnocky_statusbar_progress_set_value((gdouble) ((gdouble)found / (gdouble)count) * 100.0);
		memset(&entry, 0, sizeof(gn_phonebook_entry));
		entry.location = i;
		entry.memory_type = memory_type;
		error = gn_sm_functions(GN_OP_ReadPhonebook, &gdat, statemachine);
		if (error != GN_ERR_NONE && error != GN_ERR_INVALIDLOCATION &&
		    error != GN_ERR_EMPTYLOCATION && error != GN_ERR_INVALIDMEMORYTYPE) {
			gint err_count = 0;

			while (error != GN_ERR_NONE) {
				g_print("%s: line %d: Can't get memory entry number %d from memory %d! %d\n",
					__FILE__, __LINE__, i, entry.memory_type, error);
				if (err_count++ > 3) {
					g_mutex_unlock(phonebook_mutex);
					return error;
				}
				error = gn_sm_functions(GN_OP_ReadPhonebook, &gdat, statemachine);
				sleep(1);
			}
		} else if (error == GN_ERR_INVALIDMEMORYTYPE) {
			g_mutex_unlock(phonebook_mutex);
			return error;
		} else if (error == GN_ERR_NONE) {
			if (!entry.empty) {
				gn_phonebook_entry *en = g_new(gn_phonebook_entry, 1);
				memcpy(en, &entry, sizeof(gn_phonebook_entry));
				phonebook_list = g_slist_append(phonebook_list, en);
				found++;
			}
		} else if (error == GN_ERR_EMPTYLOCATION) {
			error = GN_ERR_NONE;
		}
		i++;
	}
	gnocky_statusbar_progress_set_value(100.0);
	g_mutex_unlock(phonebook_mutex);
	return error;
}

static void gnocky_free_phonebook_entry(gpointer data, gpointer user_data)
{
	gn_phonebook_entry *entry = data;

	g_free(entry);
}

static gn_error gnocky_read_phonebook()
{
	gn_error error = GN_ERR_NONE;
	
	error = gnocky_phonebook_memory_status();
	
	g_mutex_lock(phonebook_mutex);
	
	phonebook_updated = TRUE;
	g_slist_foreach(phonebook_list, gnocky_free_phonebook_entry, NULL);
	g_slist_free(phonebook_list);
	phonebook_list = NULL;
	
	g_mutex_unlock(phonebook_mutex);
	
	gnocky_statusbar_progress_show();

	/* sim card memory */
	if (pm.ms_sm_free != -1) {
		gnocky_statusbar_set_text(_("Reading SIM Card phonebook memory"));
		error = gnocky_read_phonebook_mem(pm.ms_sm_used, pm.ms_sm_used + pm.ms_sm_free, GN_MT_SM);
		gnocky_statusbar_set_text(NULL);
	}
	
	/* mobile memory*/
	if (pm.ms_me_free != -1) {
		gnocky_statusbar_set_text(_("Reading mobile internal phonebook memory"));
		error = gnocky_read_phonebook_mem(pm.ms_me_used, pm.ms_me_used + pm.ms_me_free, GN_MT_ME);
		gnocky_statusbar_set_text(NULL);
	}
	

	gnocky_statusbar_progress_hide();
	
	return error;
}

static gn_error gnocky_write_phonebook_entry(gn_phonebook_entry *entry)
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;

	gn_data_clear(&gdat);
	if (!entry) 
		return error;

	gdat.phonebook_entry = entry;

	if ((error = gn_sm_functions(GN_OP_WritePhonebook, &gdat, statemachine)) != GN_ERR_NONE) {
		return error;
	} else {
		phonebook_updated = TRUE;
	}
	
	return error;
}


static gn_error gnocky_add_phonebook_entry(gn_phonebook_entry *entry)
{
	gn_error error = GN_ERR_NONE;

	if (!entry) 
		return error;

	g_mutex_lock(phonebook_mutex);

	/* location == 0 means find an empty location */
	if (!entry->location) {
		gnocky_statusbar_set_text(_("Finding free phonebook location..."));
		error = gnocky_find_empty_location(entry->memory_type, &entry->location);
	}
	if (error == GN_ERR_NONE) {
		gnocky_statusbar_set_text(_("Adding phonebook entry..."));
		error = gnocky_write_phonebook_entry(entry);
	}
	phonebook_list = g_slist_append(phonebook_list, entry);

	gnocky_statusbar_set_text(NULL);
	g_mutex_unlock(phonebook_mutex);

	gnocky_phonebook_memory_status();

	return error;
}

static gn_error gnocky_delete_phonebook_entry(gn_phonebook_entry *entry)
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;

	gn_data_clear(&gdat);

	if (!entry) 
		return error;

	gdat.phonebook_entry = entry;
	entry->empty = TRUE;
	
	g_mutex_lock(phonebook_mutex);
	
	if ((error = gn_sm_functions(GN_OP_DeletePhonebook, &gdat, statemachine)) == GN_ERR_NOTIMPLEMENTED) {
		entry->name[0] = 0;
		entry->number[0] = 0;

		error = gnocky_write_phonebook_entry(entry);
	}
	
	phonebook_list = g_slist_remove(phonebook_list, entry);
	gnocky_free_phonebook_entry(entry, NULL);
	
	phonebook_updated = TRUE;
	g_mutex_unlock(phonebook_mutex);
	
	gnocky_phonebook_memory_status();
	
	return error;
}

static gn_error gnocky_write_phonebook()
{
	gn_error error = GN_ERR_NONE;
	GSList *tmp;
	gint written = 0, count;
	
	g_mutex_lock(phonebook_mutex);
	
	count = g_slist_length(phonebook_list);
	
	gnocky_statusbar_progress_show();
	gnocky_statusbar_progress_set_value(0.0);
	gnocky_statusbar_set_text(_("Writing phonebook to phone..."));
	
	tmp = phonebook_list;
	while (tmp) {
		error = gnocky_write_phonebook_entry((gn_phonebook_entry *)tmp->data);
		if (error != GN_ERR_NONE)
			break;
		written++;
		gnocky_statusbar_progress_set_value((gdouble) ((gdouble)written / (gdouble)count) * 100.0);
		tmp = tmp->next;
	}
	
	gnocky_statusbar_progress_set_value(100.0);
	gnocky_statusbar_progress_hide();
	gnocky_statusbar_set_text("");
	
	g_mutex_unlock(phonebook_mutex);

	return error;
}
static gn_error gnocky_read_bitmap(gn_bmp_types type) 
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;
	pm.bitmap.type = type;
	gn_data_clear(&gdat);
	gnocky_statusbar_set_text(_("Reading bitmap from phone..."));
	gdat.bitmap = &pm.bitmap;
	if ((error = gn_sm_functions(GN_OP_GetBitmap, &gdat, statemachine)) != GN_ERR_NONE) {
		return error;
	}
	gnocky_copy_bmp_to_screen(&pm.bitmap);
	gnocky_statusbar_set_text("");
	return error;
}

static gn_error gnocky_write_bitmap(gn_bmp_types type) 
{
	gn_error error = GN_ERR_NONE;
	gn_network_info netinfo;
	gn_data gdat;
	
	pm.bitmap.type = type;
	gnocky_statusbar_set_text(_("Writing bitmap to phone..."));
	
	gn_data_clear(&gdat);
	memset(&netinfo, 0, sizeof(netinfo));
	gdat.network_info = &netinfo;
    	
	switch (pm.bitmap.type) {
		
		case GN_BMP_OperatorLogo:
			gdat.reg_notification = NULL;
			gdat.callback_data = NULL;
			if (gn_sm_functions(GN_OP_GetNetworkInfo, &gdat, statemachine) == GN_ERR_NONE) {
				strcpy(pm.bitmap.netcode, netinfo.network_code);
				g_print("NETCODE %s\n", netinfo.network_code);
			}
			if (!strncmp(statemachine->driver.phone.models, "6510", 4))
				gn_bmp_resize(&pm.bitmap, GN_BMP_NewOperatorLogo, &statemachine->driver.phone);
			else 
				gn_bmp_resize(&pm.bitmap, GN_BMP_OperatorLogo, &statemachine->driver.phone);
			break;
		default:
			break;
	}
	gn_data_clear(&gdat);
	gdat.bitmap = &pm.bitmap;
	
	if ((error = gn_sm_functions(GN_OP_SetBitmap, &gdat, statemachine)) != GN_ERR_NONE) {
		return error;
	}
	gnocky_copy_bmp_to_screen(&pm.bitmap);
	gnocky_statusbar_set_text("");
	return error;
}

static void gnocky_free_sms_entry(gpointer data, gpointer user_data)
{
	gn_sms *sms = data;

	g_free(sms);
}

static gn_error gnocky_read_sms_list_mt(gchar *current_mt)
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;
	gn_sms_status smsstatus = {0, 0, 0, 0};
	gn_sms_folder folder;
	gn_sms_folder_list folderlist;
	gn_memory_status memory_status = {0, 0, 0};
	gn_sms message;
	gint count = 0, i = 1;
	gchar *status_text = NULL;

	folder.folder_id = 0;
	gdat.sms_folder = &folder;
	gdat.sms_folder_list = &folderlist;
	gdat.sms_status = &smsstatus;
	gdat.memory_status = &memory_status;
	memory_status.memory_type = gn_str2memory_type(current_mt);

	gnocky_statusbar_progress_show();
	gnocky_statusbar_progress_set_value(0.0);
	gnocky_statusbar_set_text(_("Reading SMS status..."));

	error = gn_sm_functions(GN_OP_GetSMSStatus, &gdat, statemachine);
	if (error == GN_ERR_NONE) {
		g_print("SMS Messages: %s Unread %d Total %d\n", current_mt, smsstatus.unread, smsstatus.number);
	
		status_text = g_strdup_printf(_("Reading SMS list from %s memory..."), current_mt);
		gnocky_statusbar_set_text(status_text);
		while (count < smsstatus.number) {
			memset(&message, 0, sizeof(gn_sms));
			message.memory_type = gn_str2memory_type(current_mt);
			message.number = i;
			gdat.sms = &message;
			if ((error = gn_sms_get(&gdat, statemachine)) == GN_ERR_NONE) {
				gn_sms *sms = g_new0(gn_sms, 1);
				memcpy(sms, &message, sizeof(gn_sms));
				sms_list = g_slist_append(sms_list, sms);
				count++;
			} else if (error == GN_ERR_EMPTYLOCATION) {
				error = GN_ERR_NONE;
			} else {
				/* with some phones/drivers smsstatus.number sums messages from all memories,
				   so ignore the error if we read at least one SMS from the current memory */
				if ((error == GN_ERR_INVALIDLOCATION) && (count > 0)) {
					error = GN_ERR_NONE;
				}
				break;
			}
			i++;
			gnocky_statusbar_progress_set_value((gdouble) ((gdouble)count / (gdouble)smsstatus.number) * 100.0);
		}
		g_free(status_text);
	}

	gnocky_statusbar_progress_set_value(100.0);
	gnocky_statusbar_progress_hide();
	gnocky_statusbar_set_text("");

	return error;
}

static gn_error gnocky_read_sms_list()
{
	gn_error error = GN_ERR_NONE;

	g_mutex_lock(sms_mutex);

	sms_updated = TRUE;
	g_slist_foreach(sms_list, gnocky_free_sms_entry, NULL);
	g_slist_free(sms_list);
	sms_list = NULL;

	error = gnocky_read_sms_list_mt("ME");
	if ((error == GN_ERR_NONE) || (error == GN_ERR_INVALIDMEMORYTYPE)) {
		error = gnocky_read_sms_list_mt("IN");
		if (error == GN_ERR_INVALIDMEMORYTYPE) {
			error = gnocky_read_sms_list_mt("SM");
		}
	}

	g_mutex_unlock(sms_mutex);

#if 1
	/* FIXME if there is an error sometimes the GUI locks up later after returning from this function */
	if (error != GN_ERR_NONE) {
		g_print("%s: %s\n", __FUNCTION__, gn_error_print(error));
		error = GN_ERR_NONE;
	}
#endif
	return error;
}

static gn_error gnocky_send_sms(gn_sms *sms)
{
	gn_error error = GN_ERR_NONE;
	gn_data gdat;
	
	gn_data_clear(&gdat);
	
	if (!sms->smsc.number[0]) {
                gdat.message_center = calloc(1, sizeof(gn_sms_message_center));
                gdat.message_center->id = 1;
                if (gn_sm_functions(GN_OP_GetSMSCenter, &gdat, statemachine) == GN_ERR_NONE) {
                        strcpy(sms->smsc.number, gdat.message_center->smsc.number);
                        sms->smsc.type = gdat.message_center->smsc.type;
                }
                g_free(gdat.message_center);
        }

        if (!sms->smsc.type) sms->smsc.type = GN_GSM_NUMBER_Unknown;

	
	gdat.sms = sms;
	
	if ((error = gn_sms_send(&gdat, statemachine) != GN_ERR_NONE)) {
		g_print("ERROR: %s\n", gn_error_print(error));
	}
	g_free(sms->reference);
	g_free(sms);
	
	return error;
}

static gn_error gnocky_delete_sms(gn_sms *sms)
{
	gn_error error = GN_ERR_NONE;
	gn_sms_folder folder;
	gn_sms_folder_list folder_list;
	gn_data gdat;
	
	gn_data_clear(&gdat);
	
	gdat.sms = sms;
	gdat.sms_folder = &folder;
	gdat.sms_folder_list = &folder_list;
	
	if ((error = gn_sms_delete(&gdat, statemachine) != GN_ERR_NONE)) {
		g_print("ERROR: %s\n", gn_error_print(error));
		return error;
	}

	g_mutex_lock(sms_mutex);

	sms_updated = TRUE;
	sms_list = g_slist_remove(sms_list, sms);
	g_free(sms);
	
	g_mutex_unlock(sms_mutex);
	
	return error;
}

gn_error monitor_terminate_link()
{
	gn_error error;

	gnocky_statusbar_set_text(_("Closing link..."));
	error = gn_sm_functions(GN_OP_Terminate, NULL, statemachine);

	gn_lib_phone_close(statemachine);
	gn_lib_phoneprofile_free(&statemachine);
	gn_lib_library_free();

	return error;
}

static 
void gnocky_monitor_terminate()
{
	gn_error error;

	monitor_terminate_link();
	pm.link_status = GNOCKY_LINK_IDLE;

	g_free(pm.manufacturer);
	g_free(pm.model);
	g_free(pm.revision);
	g_free(pm.imei);
	g_free(pm.network_name);
	g_free(pm.network_country);

	g_thread_exit(NULL);
}

static gn_error gnocky_execute_event(GnockyEvent *ev)
{
	gn_error error = GN_ERR_NONE;
	
	if (pm.link_status != GNOCKY_LINK_CONNECTED) {
		gnocky_show_error_from_thread(_("Phone is not connected!"));
		return error;
	}
	
	switch (ev->type) {
		case GNOCKY_EVENT_READ_PHONEBOOK:
			error = gnocky_read_phonebook();
			break;
		case GNOCKY_EVENT_WRITE_PHONEBOOK:
			error = gnocky_write_phonebook();
			break;
		case GNOCKY_EVENT_WRITE_PHONEBOOK_ENTRY:
			error = gnocky_write_phonebook_entry(ev->data);
			break;
		case GNOCKY_EVENT_ADD_PHONEBOOK_ENTRY:
			error = gnocky_add_phonebook_entry(ev->data);
			break;
		case GNOCKY_EVENT_DELETE_PHONEBOOK_ENTRY:
			error = gnocky_delete_phonebook_entry(ev->data);
			break;
		case GNOCKY_EVENT_READ_BITMAP:
			error = gnocky_read_bitmap((gn_bmp_types) ev->data);
			break;
		case GNOCKY_EVENT_WRITE_BITMAP:
			error = gnocky_write_bitmap((gn_bmp_types) ev->data);
			break;
		case GNOCKY_EVENT_READ_SMS_LIST:
			error = gnocky_read_sms_list();
			break;
		case GNOCKY_EVENT_SEND_SMS:
			error = gnocky_send_sms(ev->data);
			break;
		case GNOCKY_EVENT_DELETE_SMS:
			error = gnocky_delete_sms(ev->data);
			break;
		case GNOCKY_EVENT_NONE:
			break;
		case GNOCKY_EVENT_TERMINATE:
			/* FIXME the caller can't free ev because this call never returns */
			gnocky_monitor_terminate();
			break;
		default:
			g_print("UNKNOWN EVENT\n");
			break;
	}
	return error;
}

static 
gn_error gnocky_get_basic_phone_info()
{
	gn_error error = GN_ERR_NONE;
    	gn_data gdat;

	gn_data_clear(&gdat);
	gnocky_statusbar_set_text(_("Reading phone info..."));

	g_free(pm.model);
	pm.model = g_strdup(gn_lib_get_phone_model(statemachine));
	g_free(pm.manufacturer);
	pm.manufacturer = g_strdup(gn_lib_get_phone_manufacturer(statemachine));
	g_free(pm.revision);
	pm.revision = g_strdup(gn_lib_get_phone_revision(statemachine));
	g_free(pm.imei);
	pm.imei = g_strdup(gn_lib_get_phone_imei(statemachine));
	
	gnocky_statusbar_set_text(NULL);
	
	memcpy(&pm.info, &statemachine->driver.phone, sizeof(pm.info));

	phoneview_updated = TRUE;

	return error;
}

gn_error monitor_initialise_link()
{
	gn_error error;

	if (!phonebook_mutex)
		gnocky_monitor_init();

	error = gn_lib_phoneprofile_load_from_file(NULL, NULL, &statemachine);
	if (error != GN_ERR_NONE) {
		gnocky_statusbar_set_text(_("Cannot load phone!"));
		return error;
	}

	gnocky_statusbar_set_text(_("Connecting..."));
	error = gn_lib_phone_open(statemachine);
	if (error != GN_ERR_NONE) {
		gnocky_statusbar_set_text(_("Connection failed!"));
		return error;
	}

	return gnocky_get_basic_phone_info();
}

static
gpointer gnocky_monitor_loop(gpointer user_data)
{
	GTimeVal tv;
    	GnockyEvent *ev;
	gn_rf_unit rf_units = GN_RF_Percentage;
	gn_battery_unit batt_units = GN_BU_Percentage;
    	gn_data gdat;
	gn_error error = GN_ERR_NONE;
	gn_network_info networkinfo;
	
	gn_data_clear(&gdat);
	gdat.rf_level = &pm.rf_level;
	gdat.rf_unit = &rf_units;
	gdat.power_source = &pm.power_source;
	gdat.battery_unit = &batt_units;
	gdat.battery_level = &pm.battery_level;
	gdat.network_info = &networkinfo;
	
	while (1) {
		g_get_current_time(&tv);
		tv.tv_sec += 3;
		ev = g_async_queue_timed_pop(events, &tv);
		if (ev) {
		   	if ((error = gnocky_execute_event(ev)) != GN_ERR_NONE)
				gnocky_show_warning_from_thread(gn_error_print(error));
			g_free(ev);
		}

		g_mutex_lock(phoneview_mutex);

		if (pm.link_status != GNOCKY_LINK_CONNECTED) {
			if ((error = monitor_initialise_link()) != GN_ERR_NONE) {
				gnocky_set_button_active(FALSE);
				monitor_terminate_link();
				pm.link_status = GNOCKY_LINK_FAILED;
				g_mutex_unlock(phoneview_mutex);
				continue;
			} else {
				pm.link_status = GNOCKY_LINK_CONNECTED;
				gnocky_set_button_active(TRUE);
			}
		}

		gdat.reg_notification = NULL;
		gdat.callback_data = NULL;
		if ((error = gn_sm_functions(GN_OP_GetNetworkInfo, &gdat, statemachine)) != GN_ERR_NONE) {
			g_free(pm.network_name);
			pm.network_name = NULL;
			g_free(pm.network_country);
			pm.network_country = NULL;
			switch (error) {
			case GN_ERR_NOTIMPLEMENTED:
			case GN_ERR_NOTAVAILABLE:
				/* non fatal errors */
				break;
			default:
				/* fatal errors */
				gnocky_statusbar_set_text(_("Connection broken!"));
				gnocky_set_button_active(FALSE);
				monitor_terminate_link();
				pm.link_status = GNOCKY_LINK_FAILED;
				g_mutex_unlock(phoneview_mutex);
				continue;
			}
		} else {
			g_free(pm.network_name);
			g_free(pm.network_country);
			pm.network_name = g_strdup(gn_network_name_get(networkinfo.network_code));
			pm.network_country = g_strdup(gn_country_name_get(networkinfo.network_code));
		}
		
		if (gn_sm_functions(GN_OP_GetRFLevel, &gdat, statemachine) != GN_ERR_NONE) {
			pm.rf_level = -1;
		} else {
			if (rf_units == GN_RF_Arbitrary)
				pm.rf_level *= 25;
			else if (rf_units == GN_RF_CSQ) {
				if (pm.rf_level >= 0 && pm.rf_level < 99) {
					pm.rf_level *= RF_LEVEL_CSQ_SCALING;
				} else if (pm.rf_level >= 99) {
					pm.rf_level = -1;
				}
			}
		}

		if (gn_sm_functions(GN_OP_GetPowersource, &gdat, statemachine) != GN_ERR_NONE) {
			pm.power_source = -1;
		}

		if (gn_sm_functions(GN_OP_GetBatteryLevel, &gdat, statemachine) != GN_ERR_NONE) {
			pm.battery_level = -1;
		} else if (batt_units == GN_BU_Arbitrary) {
			pm.battery_level *= 25;
		}

		g_mutex_unlock(phoneview_mutex);
	}
}

void gnocky_monitor_create()
{
	gnocky_monitor_init();
	monitor = g_thread_create((GThreadFunc) gnocky_monitor_loop, NULL, TRUE, NULL);
}

void gnocky_monitor_destroy()
{
	if (pm.link_status == GNOCKY_LINK_CONNECTED) {
		if (monitor) {
			gnocky_event_push(GNOCKY_EVENT_TERMINATE, NULL);

			g_print("Waiting to sync with monitor thread...\n");
			g_thread_join(monitor);
			g_print("Sync OK. Exiting!\n");

			if (phoneview_mutex)
				g_mutex_free(phoneview_mutex);
			if (phonebook_mutex)
				g_mutex_free(phonebook_mutex);
			if (sms_mutex)
				g_mutex_free(sms_mutex);
		}
	} else {
		g_print("Not connected. Exiting!\n");
	}

}
