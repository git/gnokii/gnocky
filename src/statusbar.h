/* $Id$ */

void gnocky_statusbar_init();
void gnocky_statusbar_terminate();
void gnocky_statusbar_progress_hide();
void gnocky_statusbar_progress_show();
void gnocky_statusbar_update();
void gnocky_statusbar_progress_set_value(gdouble value);
void gnocky_statusbar_set_text(gchar *text);
