/* $Id$ */

#ifndef _GNOCKY_H_
#define _GNOCKY_H_

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <gnokii.h>

#include "events.h"

// CSQ Units scaling (scale from -113 dBm to -51 dBm)
#define RF_LEVEL_CSQ_SCALING (100.0 / 33.0)

/* status of link to phone */

enum {
	GNOCKY_LINK_IDLE,
	GNOCKY_LINK_CONNECTING,
	GNOCKY_LINK_CONNECTED,
	GNOCKY_LINK_FAILED,
	GNOCKY_LINK_TERMINATE
};

/* why we duplicate gn_phonebook_entry? i don't know... yet */
/*
typedef struct {
	gint location;
	gn_memory_type memory_type;
	gchar *name;
	gchar *number;
	GSList *subentries;
} GnockyPhoneBookEntry;
*/

typedef struct {
	int type;
	gpointer data;
} GnockyEvent;

typedef struct {
	gint link_status;
	float rf_level;
	float battery_level;
	gchar *manufacturer;
	gchar *model;
	gchar *revision;
	gchar *imei;
	gn_power_source power_source;
	gchar *network_name;
	gchar *network_country;
	gint ms_me_free;
	gint ms_me_used;
	gint ms_sm_free;
	gint ms_sm_used;
	gn_bmp bitmap;
	gn_phone info;
} GnockyPhoneMonitor;

typedef struct {
	GtkWidget *statusbar;
	GtkWidget *progress;
	gchar *text;
	gchar *progress_text;
	gboolean progress_show;
	gdouble progress_value;
	gint timeout;
} GnockyStatusBar;

typedef struct {
	gint type;
	gchar *str;
} GnockyMessage;

typedef struct {
	GtkWindow *parent;
	GtkWidget *sms_number;
} GnockyNewSmsInfo;

#endif
