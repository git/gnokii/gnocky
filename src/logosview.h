/* $Id$ */

#include <gtk/gtk.h>
#include "gnocky.h"

GtkWidget *gnocky_logos_view_create();
void gnocky_logos_view_destroy(GtkWidget *logos_view);
void gnocky_logos_view_update(GnockyPhoneMonitor pm);
void gnocky_copy_bmp_to_screen(gn_bmp *bmp);

#define TILE_SIZE 5

#define DEFAULT_XSIZE 78
#define DEFAULT_YSIZE 21

enum {
	DM_NONE = 0x0,
	DM_POINT = 0x01,
	DM_LINE = 0x02,
	DM_RECT = 0x04,
	DM_ELLIPSE = 0x08,
	DM_FILL = 0x10,
	DM_DEFAULT = DM_POINT
};
